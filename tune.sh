#!/bin/bash

#Decide other params
#typeOfAnalysis='accuracy' #performance or accuracy
#method='original' #original, strided, or recompute
#num_images=61 #total number of frames we want to run on
#stride=10 #stride between images
#currentStridedFrame=0 #The frame we are using
#currentRecomputeFrame=1 #Another frame
#startingFrame=0 #Where to start

#GIVE THRESHOLDS THEIR INITIAL CONSERVATIVE VALUES
echo 0.1 > params/static.txt
echo 0.07 > params/dynamic.txt
echo 0.45 > params/recompute.txt

#SET PARAMS TO GET INITIAL STRIDE ACCURACY FOR BASELINE
typeOfAnalysis='accuracy' #performance or accuracy
#typeOfAnalysis='performance' #performance or accuracy
method='strided' #original, strided, or recompute
num_images=400 #total number of frames we want to run on
stride=10 #stride between images
currentStridedFrame=0 #The frame we are using
startingFrame=0 #Where to start

strideTime=0

#Execution of the model
numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $typeOfAnalysis $method $num_images $stride $startingFrame

#Do automatic performance analysis
if [ $typeOfAnalysis == 'performance' ] && [ $method == 'strided' ]
then
  #Do the thing
  strideTime=$(awk 'BEGIN{a=0} {a += $1} END{printf "%.1f", a}' orgTimings.txt)
  echo The total run time for the strided approach is $strideTime microseconds
fi

#Can do accuracy checks if so desired
if [ $typeOfAnalysis == 'accuracy' ] && [ $method == 'strided' ]
then
  avgStridedError=0
  for (( i=0; i<=num_images; i++ ))
  do
    if (( $i % $stride == 0 ))
    then
      currentStridedFrame=$i
    fi
    ground=InceptionResults/g_truth"$i".txt
    strided=InceptionResults/strided"$currentStridedFrame".txt
    cat InceptionResults/str$currentStridedFrame.txt | tr ' ' '\n' > $strided
    #echo Accuracy comparison for $ground and $strided...
    error=$(awk 'BEGIN{a=0;num=0}{getline t<'\"$ground\"'; num += 1; tmp = $0-t; a += tmp * tmp;}END{printf "%.20f", a/num}' $strided)
    #echo After running $strided, the error compared to the ground truth is: $error
    #echo
    avgStridedError=$(python -c "print ($avgStridedError + ($error/$num_images))")
  done
  echo The average error for all frames is $avgStridedError
fi

#SET PARAMS TO GET INITIAL STRIDE ACCURACY FOR BASELINE
typeOfAnalysis='accuracy' #performance or accuracy
#typeOfAnalysis='performance' #performance or accuracy
method='recompute' #original, strided, or recompute
num_images=400 #total number of frames we want to run on
stride=8 #stride between images
startingFrame=0 #Where to start
currentRecomputeFrame=$startingFrame #The frame we are using

#Execution of the model
numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $typeOfAnalysis $method $num_images $stride $startingFrame

#Do automatic performance analysis
if [ $typeOfAnalysis == 'performance' ] && [ $method == 'recompute' ]
then
  #Do the thing
  recTime=$(awk 'BEGIN{a=0} {a += $1} END{printf "%.1f", a}' recTimings.txt)
  speedUp=$(python -c "print ($strideTime / $recTime)")
  echo The total run time for my recompute approach is $recTime microseconds
  echo The speedup of the recompute approach is $speedUp
fi

if [ $typeOfAnalysis == 'accuracy' ] && [ $method == 'recompute' ]
then
  avgRecomputeError=0
  for (( i=$startingFrame; i<=$num_images+$startingFrame; i++ ))
  do
    if (( $i % $stride == 0 ))
    then
      currentRecomputeFrame=$i
    fi
    ground=InceptionResults/g_truth"$i".txt
    recompute=InceptionResults/recompute"$currentRecomputeFrame".txt

    cat InceptionResults/rec$currentRecomputeFrame.txt | tr ' ' '\n' > $recompute

    #echo Accuracy comparison for $recompute...
    error=$(awk 'BEGIN{a=0;num=0}{getline t<'\"$ground\"'; num += 1; tmp = $0-t; a += tmp * tmp;}END{printf "%.20f", a/num}' $recompute)
    #echo After running $recompute, the error compared to the $ground is: $error
    #echo
    avgRecomputeError=$(python -c "print ($avgRecomputeError + ($error/$num_images))")

  done
  echo The average error for all frames is $avgRecomputeError
fi
