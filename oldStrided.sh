#!/bin/bash

#Decide other params
#typeOfAnalysis='accuracy' #performance or accuracy
#method='original' #original, strided, or recompute
#num_images=61 #total number of frames we want to run on
#stride=10 #stride between images
#currentStridedFrame=0 #The frame we are using
#currentRecomputeFrame=1 #Another frame
#startingFrame=0 #Where to start

#GIVE THRESHOLDS THEIR INITIAL CONSERVATIVE VALUES
#echo 0.07 > params/static.txt
echo 0.1 > params/dynamic.txt
echo 0.5 > params/recompute.txt
#echo 0 > params/static.txt
#echo 0 > params/dynamic.txt
#echo 0 > params/recompute.txt

#SET PARAMS TO GET INITIAL STRIDE ACCURACY FOR BASELINE
typeOfAnalysis='performance' #performance or accuracy
method='strided' #original, strided, or recompute
num_images=450 #total number of frames we want to run on
groundStride=1 #stride for "ground truth"
strideStride=1 #stride between images
currentStridedFrame=0 #The frame we are using
startingFrame=0 #Where to start

strideTime=0

#Execution of the model
numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $typeOfAnalysis $method $num_images $strideStride $startingFrame

#Do automatic performance analysis
if [ $typeOfAnalysis == 'performance' ] && [ $method == 'strided' ]
then
  #Do the thing
  strideTime=$(awk 'BEGIN{a=0} {a += $1} END{printf "%.1f", a}' orgTimings.txt)
  echo The total run time for the strided approach is $strideTime microseconds
fi

#typeOfAnalysis='accuracy' #performance or accuracy

#Execution of the model
#numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $typeOfAnalysis $method $num_images $strideStride $startingFrame

#Can do accuracy checks if so desired
if [ $typeOfAnalysis == 'accuracy' ] && [ $method == 'strided' ]
then
  avgStridedError=0
  for (( i=0; i<=num_images; i+=groundStride ))
  do
    ground=InceptionResults/g_truth"$i".txt
    if (( $i % strideStride == 0 ))
    then
      currentStridedFrame=$i
      strided=InceptionResults/strided"$i".txt
      cat InceptionResults/str$i.txt | tr ' ' '\n' > $strided
    fi
    error=$(awk 'BEGIN{a=0;num=0}{getline t<'\"$ground\"'; num += 1; tmp = $0-t; a += tmp * tmp;}END{printf "%.20f", a/num}' $strided)
    #echo Error between $ground and $strided is $error
    avgStridedError=$(python3 -c "print ($avgStridedError + ($error/($num_images/$groundStride)))")
  done

  echo The average error for all frames is $avgStridedError
fi

#SET PARAMS TO GET INITIAL STRIDE ACCURACY FOR BASELINE
typeOfAnalysis='performance' #performance or accuracy
method='recompute' #original, strided, or recompute
num_images=450 #total number of frames we want to run on
recStride=10 #stride between images
startingFrame=0 #Where to start
currentRecomputeFrame=$startingFrame #The frame we are using

#Execution of the model
numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $typeOfAnalysis $method $num_images $recStride $startingFrame

#Do automatic performance analysis
if [ $typeOfAnalysis == 'performance' ] && [ $method == 'recompute' ]
then
  #Do the thing
  recTime=$(awk 'BEGIN{a=0} {a += $1} END{printf "%.1f", a}' recTimings.txt)
  speedUp=$(python3 -c "print ($strideTime / $recTime)")
  echo The total run time for my recompute approach is $recTime microseconds
  echo The speedup of the recompute approach is $speedUp
fi

typeOfAnalysis='accuracy' #performance or accuracy

#Execution of the model
numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $typeOfAnalysis $method $num_images $recStride $startingFrame

if [ $typeOfAnalysis == 'accuracy' ] && [ $method == 'recompute' ]
then
  avgRecomputeError=0
  for (( i=$startingFrame; i<=$num_images+$startingFrame; i+=groundStride ))
  do
    ground=InceptionResults/g_truth"$i".txt
    if (( $i % recStride == 0 ))
    then
      currentRecomputeFrame=$i
      recompute=InceptionResults/recompute"$i".txt
      cat InceptionResults/rec$i.txt | tr ' ' '\n' > $recompute
    fi

    error=$(awk 'BEGIN{a=0;num=0}{getline t<'\"$ground\"'; num += 1; tmp = $0-t; a += tmp * tmp;}END{printf "%.20f", a/num}' $recompute)
    avgRecomputeError=$(python3 -c "print ($avgRecomputeError + ($error/($num_images/$recStride)))")

  done
  echo The average error for all frames is $avgRecomputeError
fi
