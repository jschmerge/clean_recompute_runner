#!/bin/bash

#Decide other params
#typeOfAnalysis='accuracy' #performance or accuracy
#method='original' #original, strided, or recompute
#num_images=61 #total number of frames we want to run on
#stride=10 #stride between images
#currentStridedFrame=0 #The frame we are using
#currentRecomputeFrame=1 #Another frame
#startingFrame=0 #Where to start

#GIVE THRESHOLDS THEIR INITIAL CONSERVATIVE VALUES
echo 1.05 > params/static.txt
echo 0.05 > params/dynamic.txt
echo 0.5 > params/recompute.txt

#SET PARAMS TO GET INITIAL STRIDE ACCURACY FOR BASELINE
method='recompute' #original, strided, or recompute
num_images=499 #total number of frames we want to run on
#num_images=10 #total number of frames we want to run on
groundStride=1 #stride for "ground truth"
startingFrame=0 #Where to start
#video='Parking'
video='Town'
model='Inception'
#model='Resnet'

echo "Recompute Results" > "$model""$video"Results/recResults.txt

for (( q=1; q < 11; q += 1 ))
#for (( q=3; q < 5; q += 1 ))
do
  echo Doing stride $q
  currentRecomputeFrame=$startingFrame #The frame we are using
  recTime=0
  recStride=$q #stride between images

  #Execution of the model
  if [ $model == 'Inception' ]
  then
    numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $method $num_images $recStride $startingFrame $video $model
  fi

  if [ $model == 'Resnet' ]
  then
    numactl --cpubind=3 --membind=3 ./bin/run_graph models/newresnet50/model $method $num_images $recStride $startingFrame $video $model
  fi

  #Do automatic performance analysis
  recTime=$(awk 'BEGIN{a=0} {a += $1} END{printf "%.1f", a}' recTimings.txt)
  #echo The total run time for my recompute approach is $recTime microseconds
  avgRecomputeError=0
  accError=0
  for (( i=$startingFrame; i<=$num_images+$startingFrame; i+=groundStride ))
  do
    ground="$model"Results/"$video"Ground/ground"$i".txt
    if (( $i % recStride == 0 ))
    then
      currentRecomputeFrame=$i
      base="$model"Results/rec"$i".txt
      temp="$model"Results/recompute"$i".txt
      cat $base | tr ' ' '\n' > $temp
      cat $temp | grep -v "^$" > $base
      rm $temp
    fi
    errorTop=$(awk 'BEGIN{acc=0}{getline t<'\"$ground\"'; tmp = $0-t; acc += tmp * tmp;}END{printf "%.20f", acc}' $base)
    errorBot=$(awk 'BEGIN{acc=0}{tmp = $0; acc += tmp * tmp;}END{printf "%.20f", acc}' $ground)
    error=$(python3 -c "print ($errorTop**(1/2.0)/($errorBot**(1/2.0)))")
    echo Error between $ground and $base is $error
    accError=$(python3 -c "print ($accError + $error)")
  done
  finalError=$(python3 -c "print ($accError/(($num_images/$groundStride)+1))")
  #echo The average error for all frames is $finalError
  #echo $q $recTime $finalError
  echo $q $recTime $finalError >> "$model""$video"Results/recResults.txt
done
