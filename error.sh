#!/bin/bash

#SET PARAMS TO GET INITIAL STRIDE ACCURACY FOR BASELINE
method='strided' #original, strided, or recompute
num_images=499 #total number of frames we want to run on
groundStride=1 #stride for "ground truth"
strideStride=500 #stride between images
currentStridedFrame=0 #The frame we are using
startingFrame=0 #Where to start

#Execution of the model
numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $method $num_images $strideStride $startingFrame 'Town' 'Inception'

#Can do accuracy checks if so desired
avgStridedError=0
accError=0
for (( i=0; i<=num_images; i+=groundStride ))
do
  ground=InceptionResults/ground"$i".txt
  if (( $i % strideStride == 0 ))
  then
    currentStridedFrame=$i
    echo Current Strided Frame is $i
    strided=InceptionResults/strided"$i".txt
    cat InceptionResults/str$i.txt | tr ' ' '\n' > $strided
    #continue
  fi
  errorTop=$(awk 'BEGIN{acc=0}{getline t<'\"$ground\"'; tmp = $0-t; acc += tmp * tmp;}END{printf "%.20f", acc}' $strided)
  errorBot=$(awk 'BEGIN{acc=0}{tmp = $0; acc += tmp * tmp;}END{printf "%.20f", acc}' $ground)
  error=$(python3 -c "print ($errorTop**(1/2.0)/($errorBot**(1/2.0)))")
  echo Error between $ground and $strided is $error
  accError=$(python3 -c "print ($accError + $error)")
done
finalError=$(python3 -c "print ($accError/(($num_images)+1))")
echo The average error for all frames is $finalError
