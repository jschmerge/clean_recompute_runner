#!/bin/bash

#GET INFO FROM USER
echo What model do you want to run? Inception/Resnet
read model
echo Which video do you want to use? Parking/Town
read video
echo What is the max acceptable accuracy loss?
read maxLoss
echo What is the max acceptable stride?
read maxStride
maxStr=$((maxStride*2))

#GIVE THRESHOLDS THEIR INITIAL CONSERVATIVE VALUES
echo 1.05 > params/static.txt
echo 0.05 > params/dynamic.txt
echo 0.5 > params/recompute.txt

#DO BASELINE ACCURACY FOR GROUND TRUTH
method='original' #original, strided, or recompute
stride=1
groundStride=1 #stride for "ground truth"
startingFrame=250 #Where to start
if [ $model == 'Inception' ]
then
  numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $method $maxStr $stride $startingFrame $video $model
fi

if [ $model == 'Resnet' ]
then
  numactl --cpubind=3 --membind=3 ./bin/run_graph models/newresnet50/model $method $maxStr $stride $startingFrame $video $model
fi

for (( i=startingFrame; i<=startingFrame+maxStr; i++ ))
do
  file="$model"Results/ground"$i".txt
  newfile="$model"Results/g_truth"$i".txt

  cat $file | tr ' ' '\n' > $newfile
  cat $newfile | grep -v "^$" > $file

  mv $file "$model"Results/"$video"Ground/
  rm $newfile

done


#SET PARAMS TO GET INITIAL STRIDE ACCURACY FOR BASELINE
method='strided' #original, strided, or recompute
strAcc=0
selectedStrideStride=0

for (( q=1; q <= maxStride; q += 1 ))
do
  echo Doing stride $q
  currentStridedFrame=$startingFrame #The frame we are using
  strideStride=$q #stride between images

  #Execution of the model
  if [ $model == 'Inception' ]
  then
    numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $method $maxStr $strideStride $startingFrame $video $model
  fi

  if [ $model == 'Resnet' ]
  then
    numactl --cpubind=3 --membind=3 ./bin/run_graph models/newresnet50/model $method $maxStr $strideStride $startingFrame $video $model
  fi

  avgStridedError=0
  accError=0
  framesRun=0
  for (( i=$startingFrame; i<=$maxStr+$startingFrame; i+=groundStride ))
  do
    ground="$model"Results/"$video"Ground/ground"$i".txt
    if (( $i % strideStride == 0 ))
    then
      currentStridedFrame=$i
      base="$model"Results/str$i.txt
      temp="$model"Results/strided"$i".txt
      cat $base | tr ' ' '\n' > $temp
      cat $temp | grep -v "^$" > $base
      rm $temp
    fi
    errorTop=$(awk 'BEGIN{acc=0}{getline t<'\"$ground\"'; tmp = $0-t; acc += tmp * tmp;}END{printf "%.20f", acc}' $base)
    errorBot=$(awk 'BEGIN{acc=0}{tmp = $0; acc += tmp * tmp;}END{printf "%.20f", acc}' $ground)
    error=$(python3 -c "print ($errorTop**(1/2.0)/($errorBot**(1/2.0)))")
    #echo Error between $ground and $base is $error
    accError=$(python3 -c "print ($accError + $error)")
    framesRun=$((framesRun+1))
  done
  finalError=$(python3 -c "print ($accError/($framesRun))")
  answer=$(python3 -c "print ($finalError > $maxLoss)" )
  echo Error for stride $q is $finalError on $framesRun images
  if [ "$answer" = "True" ]
  then
    echo $q is too high for the acc requirement
    break
  else
    strAcc=$finalError
    selectedStrideStride=$q
  fi
done

echo After initial analysis, stride of $selectedStrideStride is less than $maxStride should hit an accuracy target of $maxLoss since estimated is $strAcc

#SET PARAMS TO GET INITIAL STRIDE ACCURACY FOR BASELINE
method='recompute' #original, strided, or recompute
recAcc=0
selectedRecStride=1

for (( q=1; q <= maxStride; q += 1 ))
do
  currentRecomputeFrame=$startingFrame #The frame we are using
  recStride=$q #stride between images

  #Execution of the model
  if [ $model == 'Inception' ]
  then
    numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $method $maxStr $recStride $startingFrame $video $model
  fi

  if [ $model == 'Resnet' ]
  then
    numactl --cpubind=3 --membind=3 ./bin/run_graph models/newresnet50/model $method $maxStr $recStride $startingFrame $video $model
  fi

  avgRecomputeError=0
  accError=0
  framesRun=0
  for (( i=$startingFrame; i<=$maxStr+$startingFrame; i+=groundStride ))
  do
    ground="$model"Results/"$video"Ground/ground"$i".txt
    if (( $i % recStride == 0 ))
    then
      currentRecomputeFrame=$i
      base="$model"Results/rec"$i".txt
      temp="$model"Results/recompute"$i".txt
      cat $base | tr ' ' '\n' > $temp
      cat $temp | grep -v "^$" > $base
      rm $temp
    fi
    errorTop=$(awk 'BEGIN{acc=0}{getline t<'\"$ground\"'; tmp = $0-t; acc += tmp * tmp;}END{printf "%.20f", acc}' $base)
    errorBot=$(awk 'BEGIN{acc=0}{tmp = $0; acc += tmp * tmp;}END{printf "%.20f", acc}' $ground)
    error=$(python3 -c "print ($errorTop**(1/2.0)/($errorBot**(1/2.0)))")
    accError=$(python3 -c "print ($accError + $error)")
    framesRun=$((framesRun+1))
  done
  finalError=$(python3 -c "print ($accError/($framesRun))")
  answer=$(python3 -c "print ($finalError > $maxLoss)" )
  echo Error for stride $q is $finalError on $framesRun images
  if [ "$answer" = "True" ]
  then
    echo $q is too high for the acc requirement
    break
  else
    recAcc=$finalError
    selectedRecStride=$q
  fi
done

echo After initial analysis, stride of $selectedRecStride is less than $maxStride should hit an accuracy target of $maxLoss since estimated is $recAcc

#SET PARAMS TO GET INITIAL STRIDE ACCURACY FOR BASELINE
method='strided' #original, strided, or recompute
num_images=499 #total number of frames we want to run on
groundStride=1 #stride for "ground truth"
startingFrame=0 #Where to start

#echo "Strided Approach" > "$model""$video"Results/strResults.txt

currentStridedFrame=0 #The frame we are using
startingFrame=0 #Where to start
strideStride=$selectedStrideStride #stride between images
strideTime=0
#Execution of the model
if [ $model == 'Inception' ]
then
  numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $method $num_images $strideStride $startingFrame $video $model
fi

if [ $model == 'Resnet' ]
then
  numactl --cpubind=3 --membind=3 ./bin/run_graph models/newresnet50/model $method $num_images $strideStride $startingFrame $video $model
fi

#Do automatic performance analysis
strideTime=$(awk 'BEGIN{a=0} {a += $1} END{printf "%.1f", a}' orgTimings.txt)
#echo The total run time for the strided approach is $strideTime microseconds

#Can do accuracy checks if so desired
avgStridedError=0
accError=0
for (( i=0; i<=num_images; i+=groundStride ))
do
  ground="$model"Results/"$video"Ground/ground"$i".txt
  if (( $i % strideStride == 0 ))
  then
    currentStridedFrame=$i
    base="$model"Results/str$i.txt
    temp="$model"Results/strided"$i".txt
    cat $base | tr ' ' '\n' > $temp
    cat $temp | grep -v "^$" > $base
    rm $temp
  fi
  errorTop=$(awk 'BEGIN{acc=0}{getline t<'\"$ground\"'; tmp = $0-t; acc += tmp * tmp;}END{printf "%.20f", acc}' $base)
  errorBot=$(awk 'BEGIN{acc=0}{tmp = $0; acc += tmp * tmp;}END{printf "%.20f", acc}' $ground)
  error=$(python3 -c "print ($errorTop**(1/2.0)/($errorBot**(1/2.0)))")
  echo Error between $ground and $base is $error
  accError=$(python3 -c "print ($accError + $error)")
done
finalStrError=$(python3 -c "print ($accError/(($num_images/$groundStride)+1))")
#echo The average error for all frames is $finalError

#echo For the strided approach, a stride of $strideStride was used, it took $strideTime microseconds and the error was $finalStrError
#echo $q $strideTime $finalError >> "$model""$video"Results/strResults.txt

#SET PARAMS TO GET INITIAL STRIDE ACCURACY FOR BASELINE
method='recompute' #original, strided, or recompute

#echo "Recompute Results" > "$model""$video"Results/recResults.txt

currentRecomputeFrame=$startingFrame #The frame we are using
recTime=0
recStride=$selectedRecStride #stride between images

#Execution of the model
if [ $model == 'Inception' ]
then
  numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $method $num_images $recStride $startingFrame $video $model
fi

if [ $model == 'Resnet' ]
then
  numactl --cpubind=3 --membind=3 ./bin/run_graph models/newresnet50/model $method $num_images $recStride $startingFrame $video $model
fi

#Do automatic performance analysis
recTime=$(awk 'BEGIN{a=0} {a += $1} END{printf "%.1f", a}' recTimings.txt)
#echo The total run time for my recompute approach is $recTime microseconds
avgRecomputeError=0
accError=0
for (( i=$startingFrame; i<=$num_images+$startingFrame; i+=groundStride ))
do
  ground="$model"Results/"$video"Ground/ground"$i".txt
  if (( $i % recStride == 0 ))
  then
    currentRecomputeFrame=$i
    base="$model"Results/rec"$i".txt
    temp="$model"Results/recompute"$i".txt
    cat $base | tr ' ' '\n' > $temp
    cat $temp | grep -v "^$" > $base
    rm $temp
  fi
  errorTop=$(awk 'BEGIN{acc=0}{getline t<'\"$ground\"'; tmp = $0-t; acc += tmp * tmp;}END{printf "%.20f", acc}' $base)
  errorBot=$(awk 'BEGIN{acc=0}{tmp = $0; acc += tmp * tmp;}END{printf "%.20f", acc}' $ground)
  error=$(python3 -c "print ($errorTop**(1/2.0)/($errorBot**(1/2.0)))")
  echo Error between $ground and $base is $error
  accError=$(python3 -c "print ($accError + $error)")
done
finalError=$(python3 -c "print ($accError/(($num_images/$groundStride)+1))")
#echo The average error for all frames is $finalError
echo Experiment was on $model with $video frames, a max accuracy loss of $maxLoss and max stride of $maxStride were required
echo For the strided approach, a stride of $strideStride was used, it took $strideTime microseconds and the error was $finalStrError with an estimate of $strAcc
echo For my recompute approach, a stride of $recStride was used, it took $recTime microseconds, and the error was $finalError with an estimate of $recAcc
#echo $q $recTime $finalError >> "$model""$video"Results/recResults.txt

speedUp=$(python3 -c "print ($strideTime / $recTime)")
echo The speedup for the runner is $speedUp
