#pragma once

#include "graph.h"

enum LoadState {
  INPUT, CONST_INPUT, OP, INPUT_FEED, EDGE, NONE
};

bool update_state(std::string line, LoadState &state);

std::unordered_map<std::string, int> get_mapping();

int get_parallelism(std::string op_name);

void add_control(OpGraph &op_graph);

//template<typename T>
//void fill_from_stream(std::istream &source, T *ptr, size_t num);

TensorContainer load_tensor(std::istream &infile);

void change_graph_input(OpGraph &opGraph, std::string model_name, std::string infile, std::string model);

OpGraph load_graph(std::string model_name);

