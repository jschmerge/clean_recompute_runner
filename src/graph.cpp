#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <mutex>
#include <memory>
#include <cassert>
#include <omp.h>
#include <cmath>
#include <stdio.h>
#include <cstring>

#include "graph.h"
#include "load.h"
#include "ops/register.h"

#ifdef __INTEL_COMPILER
  //#define USE_AFFINITY_CONTROL true
  #define USE_AFFINITY_CONTROL false
#else
  #define USE_AFFINITY_CONTROL false
#endif

//Global variable for threshold
float STATIC_THRESH = 0.0;

/* AffinityControl ********************************************************************************/
const int AffinityControl::MAX_CORES = omp_get_max_threads();

std::mutex AffinityControl::lock;

std::vector<bool> AffinityControl::global_mask(MAX_CORES, false);

AffinityControl::AffinityControl() : local_mask(MAX_CORES, false) {}

void AffinityControl::set_affinity(int parallelism) {
  std::vector<bool> temp(0);
  set_affinity(parallelism, temp);
}

// TODO CPU only
void AffinityControl::set_affinity(int parallelism, const std::vector<bool> &preferred) {
#if USE_AFFINITY_CONTROL
  kmp_affinity_mask_t affinity_mask;
  kmp_create_affinity_mask(&affinity_mask);
  int quota = parallelism;
  lock.lock();
  for(int i = 0; quota > 0 && i < preferred.size(); i++) {
    if(preferred[i]) {
      if(!global_mask[i]) {
        kmp_set_affinity_mask_proc(i, &affinity_mask);
        local_mask[i] = true;
        global_mask[i] = true;
        quota--;
      }
    }
  }
  for(int i = 0; quota > 0 && i < MAX_CORES; i++) {
    if(!global_mask[i]) {
      kmp_set_affinity_mask_proc(i, &affinity_mask);
      local_mask[i] = true;
      global_mask[i] = true;
      quota--;
    }
  }
  lock.unlock();
  if(quota > 0) {
    fprintf(stderr, "(affinity) insufficient cores\n");
  } else if(kmp_set_affinity(&affinity_mask) != 0) {
    fprintf(stderr, "(affinity) error binding\n");
  }
  kmp_destroy_affinity_mask(&affinity_mask);
#endif
}

void AffinityControl::clear_affinity() {
#if USE_AFFINITY_CONTROL
  lock.lock();
  for(int i = 0; i < local_mask.size(); i++) {
    if(local_mask[i]) {
      assert(global_mask[i]);
      global_mask[i] = false;
    }
  }
  lock.unlock();
#endif
}
/* AffinityControl END ****************************************************************************/

/* OpInfo *****************************************************************************************/

OpInfo::OpInfo() : op(NULL) {
  fatal_error("Empty OpInfo");
}

OpInfo::OpInfo(const std::string op_name, const std::string instance_name, const int parallelism) :
      op(CreateOp(op_name, instance_name, parallelism)), inputs(0) {
  unsatisfied = 0;
  control_unsatisfied = 0;
}

OpInfo::OpInfo(const OpInfo &other) : op(other.op),
      inputs(other.inputs), outgoing(other.outgoing),
      outgoing_control(other.outgoing_control),
      control_unsatisfied(other.control_unsatisfied) {
  unsatisfied = other.unsatisfied.load();
}

OpInfo::~OpInfo() {}

void OpInfo::save_start_state() {
	start_state = inputs;
  start_unsatisfied = 0;
  for(size_t i = 0; i < inputs.size(); i++) {
    if(inputs[i].ttype == TT_NONE) {
      start_unsatisfied++;
    }
  }
  unsatisfied = start_unsatisfied + control_unsatisfied;
}

void OpInfo::reset() {
  inputs = start_state;
  unsatisfied = start_unsatisfied + control_unsatisfied;
}

OpGraph::OpGraph() : num_inputs(0) {}

void OpGraph::add_op(std::string op_name, std::string instance_name, int parallelism) {
  std::pair<std::string, OpInfo> p(instance_name, OpInfo(op_name, instance_name, parallelism));
  info.insert(p);
}

void OpGraph::add_data_edge(std::string one, std::string two, unsigned idx, unsigned from_idx) {
  if(info.find(one) == info.end()) fatal_error("Edge src unavailable: " + one);
  if(info.find(two) == info.end()) fatal_error("Edge dst unavailable: " + two);
  if(info[two].inputs.size() <= idx) {
    info[two].inputs.resize(idx + 1);
  }
  if(TT_NONE != info[two].inputs[idx].ttype) fatal_error("Duplication");
  auto t = std::make_tuple(&(info[two]), idx, from_idx);
  info[one].outgoing.push_back(t);
}

template<typename T1, typename T2>
void OpGraph::add_data_edge(const T1 &one, const T2 &two, unsigned idx, unsigned from_idx) {
  const Op *op_one = (const Op*)&one, *op_two = (const Op*)&two;
  add_data_edge(op_one->name(), op_two->name(), idx, from_idx);
}

void OpGraph::add_control_edge(std::string one, std::string two) {
  if(info.find(one) == info.end()) fatal_error("Edge src unavailable");
  if(info.find(two) == info.end()) fatal_error("Edge dst unavailable");
  info[one].outgoing_control.push_back(&(info[two]));
  info[two].control_unsatisfied++;
}

template<typename T1, typename T2>
void OpGraph::add_control_edge(const T1 &one, const T2 &two) {
  const Op *op_one = &one, *op_two = &two;
  add_control_edge(op_one->name(), op_two->name());
}

void OpGraph::add_input(std::string op, unsigned idx, TensorContainer &mat) {
  if(info.find(op) == info.end()) fatal_error("Target unavailable: " + op);
  if(info[op].inputs.size() <= idx) {
    info[op].inputs.resize(idx + 1);
  }
  //std::cout << op << std::endl;
  if(TT_NONE != info[op].inputs[idx].ttype) fatal_error("Duplication");
  info[op].inputs[idx] = mat;
  num_inputs++;
}

template<typename T>
void OpGraph::add_input(const T &op, unsigned idx, TensorContainer &mat) {
  const Op *op_ptr = (const Op*)&op;
  add_input(op_ptr->name(), idx, mat);
}

void OpGraph::prepare() {
  for(auto &p : info) {
    p.second.save_start_state();
  }
}

void OpGraph::reset() {
  for(auto &p : info) {
    p.second.reset();
  }
  outputs.clear();
}

std::vector<std::string> OpGraph::get_op_names() {
  std::vector<std::string> names;
  names.reserve(info.size());
  for(auto p : info) {
    names.push_back(p.first);
  }
  return names;
}

std::vector<std::pair<std::string, int>> OpGraph::get_input_names(std::string op_name) {
  std::vector<std::pair<std::string, int>> names(info[op_name].start_state.size());
  OpInfo *ptr = &(info[op_name]);
  for(auto p : info) {
    for(auto &t : p.second.outgoing) {
      OpInfo* other;
      int idx, from_idx;
      std::tie(other, idx, from_idx) = t;
      if(other == ptr) {
        names[idx] = std::make_pair(p.first, from_idx);
      }
    }
  }
  return names;
}

std::vector<TensorContainer> OpGraph::get_initialized_input(std::string op_name) {
  return info[op_name].start_state;
}

Op &OpGraph::get_op(std::string op_name) {
  return *(info[op_name].op);
}

int OpGraph::get_inter() {
  std::ifstream infile("params/inter.txt");
  int inter;
  if(infile && infile >> inter) return inter;
  else return 12;
}

template<typename T>
void OpGraph::print_tensor(const Tensor<T> &A) {
  if(A.dims() > 2) std::cout << "\nHigh Dimensional\n";
  else if(A.dims() == 2) {
    std::cout << '\n';
    for(size_t i = 0; i < A.dim(0); i++) {
      for(size_t j = 0; j < A.dim(1); j++) {
        std::cout << A.get(i, j) << ' ';
      }
      std::cout << '\n';
    }
  } else if(A.dims() == 1) {
    std::cout << '\n';
    for(size_t i = 0; i < A.dim(0); i++) {
      std::cout << A.get(i) << ' ';
    }
    std::cout << '\n';
  } else {
    std::cout << "\nZero Dimensional\n";
  }
}

template<typename T>
void OpGraph::write_tensor(std::string filename, Tensor<T> &A) {
  std::ofstream stream(filename);
  if(A.dims() > 4) stream << "\nHigh Dimensional\n";
  else if(A.dims() == 4) {
    stream << '\n';
    const T *arr_A = A.ptr();
    int spot = 0;
    for (int j = 0; j < A.dim(1); j++)
    {
      for (int k = 0; k < A.dim(2); k++)
      {
        for (int l = 0; l < A.dim(3); l++)
        {
          spot = ((j * A.dim(2) * A.dim(3)) + (k * A.dim(3)) + l);
          stream << arr_A[spot] << '\n';
        }
      }
    }
  }
  else if(A.dims() == 2) {
    stream << '\n';
    for(size_t i = 0; i < A.dim(0); i++) {
      for(size_t j = 0; j < A.dim(1); j++) {
        stream << A.get(i, j) << ' ';
      }
      stream << '\n';
    }
  } else if(A.dims() == 1) {
    stream << '\n';
    for(size_t i = 0; i < A.dim(0); i++) {
      stream << A.get(i) << ' ';
    }
    stream << '\n';
  } else {
    stream << "\nZero Dimensional\n";
  }
  stream.close();
}

void OpGraph::write_outputs_to_file(std::string filename) {
  //std::cout << filename << '\n';
  for(auto &p : outputs) {
    //std::cout << "\n" << p.first << ":";
    switch(p.second.dtype) {
    case DT_FLOAT:
      write_tensor(filename, *(p.second.t_float));
      break;
    case DT_INT:
      write_tensor(filename, *(p.second.t_int));
      break;
    default:
      break;
    }
  }
}

void OpGraph::print_outputs() {
  for(auto &p : outputs) {
    std::cout << "\n" << p.first << ":";
    switch(p.second.dtype) {
    case DT_FLOAT:
      print_tensor(*(p.second.t_float));
      break;
    case DT_INT:
      print_tensor(*(p.second.t_int));
      break;
    default:
      break;
    }
  }
}

inline void propagate_outputs(std::vector<TensorContainer> &results,
      std::vector<std::tuple<OpInfo*, unsigned, unsigned>> &outgoing,
      std::vector<OpInfo*> &outgoing_control, std::vector<OpInfo*> &now_ready) {
  now_ready.clear();
  for(auto &t : outgoing) {
    OpInfo* other;
    int idx, from_idx;
    std::tie(other, idx, from_idx) = t;
    if(results.size() <= from_idx) fatal_error("Insufficient Output");
    if(other->inputs.size() <= idx) fatal_error("Out of Bounds");
    if(other->inputs[idx].ttype != TT_NONE) fatal_error("Duplicate Data");
    other->inputs[idx] = results[from_idx];
    if(0 == --(other->unsatisfied)) {
      now_ready.push_back(other);
    }
  }
  for(OpInfo *other : outgoing_control) {
    if(0 == --(other->unsatisfied)) {
      now_ready.push_back(other);
    }
  }
}

template<typename T>
double OpGraph::euclidean(TensorContainer one, TensorContainer two) {
  const T *a = TensorExtractor<T>::Get(one)->ptr();
  const T *b = TensorExtractor<T>::Get(two)->ptr();
  size_t N = TensorExtractor<T>::Get(one)->elements();
  if(N != TensorExtractor<T>::Get(two)->elements()) return 999.0;
  double sum = 0.0;
  int count = 0;
  for(size_t i = 0; i < N; i++) {
    T diff = std::abs (a[i] - b[i]);
    if (diff > 0.0000001)
    {
      //std::cout << "Diff is " << diff << " on a single value \n";
      //std::cout << "First " << a[i] << " Second " << b[i] << " on spot " << i << std::endl;
      count++;
    }
    sum += diff;
  }
  if (count > 0)
  {
    std::cout << "Op had " << count << " over the threshold \n";
    //exit(0);
  }
  return sqrt(sum);
}

double OpGraph::get_error(TensorContainer one, TensorContainer two) {
  if(one.dtype != two.dtype) return 999.0;
  switch(one.dtype) {
  case DT_FLOAT: return euclidean<float>(one, two);
  case DT_INT: return euclidean<int>(one, two);
  case DT_LONG: return euclidean<long>(one, two);
  default: return -1;
  }
}

void OpGraph::change_input(std::string op, unsigned idx, TensorContainer &mat) {
  if(info.find(op) == info.end()) fatal_error("Target unavailable: " + op);
  if(info[op].inputs.size() <= idx) {
    info[op].inputs.resize(idx + 1);
  }
  bool check = false;
  if(TT_NONE != info[op].inputs[idx].ttype) check = true;
  if (!check) fatal_error("Value not already exists");
#if !defined(NDEBUG) && !defined(NLOADDEBUG)
  printf("add_input called on info[%s].inputs[%d] \n", 
          op.c_str(), idx);
#endif
  info[op].inputs[idx] = mat;
}

//My function to take in 2 tensors and compute the bounding boxes that capture the
//differences between them
std::vector<std::vector<int> > compare(const float* oldT, const float* newT, int num_boxes_x, int num_boxes_y, int input_height, int input_width, int input_depth)
{
  int parallelism = 12;
  int numValues = input_height * input_width * input_depth;
  float avg_diff = 0.0;
  float tmp1 = 0.0;

  //Computes the avg difference between all pixels
  for (int i = 0; i < (input_width*input_height*input_depth); i++)
  {
    tmp1 = std::max(oldT[i], newT[i]);
    if (tmp1 != 0.0) {
      avg_diff += std::abs(newT[i]-oldT[i])/tmp1;
    }
  }

  //The static threshold is based on this average
  STATIC_THRESH = avg_diff / (input_width * input_height * input_depth);
  if (STATIC_THRESH > 0.15)
  {
    STATIC_THRESH = 0.0;
  }

  //Read in Static Threshold multiplier from file
  float STATIC_MULT = 0.0;
  std::ifstream infile("params/static.txt");
  if(!infile) fatal_error("Cannot open static");
  std::string line;
  infile >> line;
  STATIC_MULT = std::stof (line);
  //std::cout << "Using static recompute threshold of  " << STATIC_THRESH << " and multiplier of " << STATIC_MULT << '\n';

  std::vector<std::vector<int> > allBoxes;
  int totalBoxes = num_boxes_x * num_boxes_y;
  int box_width = input_width / num_boxes_x;
  int box_height = input_height / num_boxes_y;
  int* updateInfo = (int*)std::malloc(totalBoxes * sizeof(int));
  bool merged = false;
  int firstCornerX = 0;
  int firstCornerY = 0;
  int firstAffectedWidth = 0;
  int firstAffectedHeight = 0;
  int xOverlap = 0;
  int newCornerX = 0;
  int newCornerY = 0;
  int newAffectedWidth = 0;
  float thresh = STATIC_THRESH * STATIC_MULT * box_width * box_height * input_depth;

  //Loop to go through each sub box and see if they have changed
  #pragma omp parallel for num_threads(parallelism)
  for (int i = 0; i < totalBoxes; i++)
  {
    //Will be which element in updateInfo you change
    int x = i % num_boxes_x;
    int y = i / num_boxes_x;
    int x_start = x * box_width;
    int y_start = y * box_height;
    int x_end = 0;
    int y_end = 0;

    //Bound our end at the edges of the input
    x_end = (x_start + box_width < input_width) ? (x_start + box_width) : (input_width);
    y_end = (y_start + box_height < input_height) ? (y_start + box_height) : (input_height);

    float diff = 0.0;
    int spot = 0;
    double tmp = 0.0;
    float oldVal = 0.0;
    float newVal = 0.0;
    float aboveOld = 0.0;
    float belowOld = 0.0;
    float currentOld = 0.0;
    float aboveNew = 0.0;
    float belowNew = 0.0;
    float currentNew = 0.0;
    for (int j = y_start; j < y_end; j++)
    {
      for (int k = x_start; k < x_end; k++)
      {
        for (int l = 0; l < input_depth; l++)
        {
          spot = ((j * input_width * input_depth) + (k * input_depth) + l);

          if (k == 0 || j == 0 || (k == (input_width-1)) || (j == (input_height-1)) )
          {
            //Skip these iterations, we can't apply a 3x3 kernel to them
            oldVal = oldT[spot];
            newVal = newT[spot];
          }
          else 
          {
            //Do blur here and put value into blurredTensor
            aboveOld = (oldT[(spot - (input_width * input_depth) - input_depth)] * 1) + 
                       (oldT[(spot - (input_width * input_depth) + input_depth)] * 1) +
                       (oldT[(spot - (input_width * input_depth))] * 2);
            belowOld = (oldT[(spot + (input_width * input_depth) - input_depth)] * 1) +
                       (oldT[(spot + (input_width * input_depth) + input_depth)] * 1) +
                       (oldT[(spot + (input_width * input_depth))] * 2);
            currentOld = (oldT[spot - input_depth] * 2) + (oldT[spot + input_depth] * 2) + (oldT[spot] * 4);
            oldVal = (aboveOld + belowOld + currentOld) / 16;

            //Do blur here and put value into blurredTensor
            aboveNew = (newT[(spot - (input_width * input_depth) - input_depth)] * 1) +
                       (newT[(spot - (input_width * input_depth) + input_depth)] * 1) +
                       (newT[(spot - (input_width * input_depth))] * 2);
            belowNew = (newT[(spot + (input_width * input_depth) - input_depth)] * 1) +
                       (newT[(spot + (input_width * input_depth) + input_depth)] * 1) +
                       (newT[(spot + (input_width * input_depth))] * 2);
            currentNew = (newT[spot - input_depth] * 2) + (newT[spot + input_depth] * 2) + (newT[spot] * 4);
            newVal = (aboveNew + belowNew + currentNew)/16;
          }
          //tmp = std::max(newT[spot], oldT[spot]);
          tmp = std::max(newVal, oldVal);

          //Avoid a divide by 0 for relative change
          if (tmp != 0.0) {
            //diff += (std::abs(newT[spot]-oldT[spot]))/tmp;
            diff += (std::abs(newVal-oldVal))/tmp;
          }
        }
      }
    }

    //If box needs updated, it's a 1, otherwise 0
    updateInfo[i] = (diff > thresh) ? 1 : 0;
  }

  /*for (int l = 0; l < totalBoxes; l++)
  {
    if (l % num_boxes_x == 0)
    {
      std::cout << std::endl;
    }
    std::cout << updateInfo[l];
  }
  std::cout << std::endl << std::endl; */

  int update = 0;
  int x = 0;
  int y = 0;
  int num_merged_boxes = 0;
  int lasty = 0;
  int cornerX = 0;
  int cornerY = 0;
  bool makingBox = false;
  std::vector<int> temp;

  //Going to do merge along rows, then see if we can merge that box in with any existing
  //boxes that we have already made
  for (int i = 0; i < totalBoxes; i++)
  {
    x = i % num_boxes_x;
    y = i / num_boxes_x;
    merged = false;

    if (y == lasty && makingBox)
    {
      //Doesn't help?
      //if (updateInfo[i] == 1 || updateInfo[i+1] == 1 || updateInfo[i+2] == 1)
      if (updateInfo[i] == 1 || updateInfo[i+1] == 1)
      //if (updateInfo[i] == 1)
      {
        //Continue making the current box, allow up to 2 zeros before closing box
        num_merged_boxes++;
      }
      else
      {
        //End that box and push it back
        for (int q = 0; q < allBoxes.size(); q++)
        {
          firstCornerX = allBoxes[q][0];
          firstCornerY = allBoxes[q][1];
          firstAffectedWidth = allBoxes[q][2];
          firstAffectedHeight = allBoxes[q][3];
          newCornerX = cornerX * box_width;
          newCornerY = cornerY * box_height;
          newAffectedWidth = num_merged_boxes * box_width;
          if (newCornerY == (firstCornerY + firstAffectedHeight))
          {
            xOverlap = (std::min((firstCornerX + firstAffectedWidth), (newCornerX + newAffectedWidth)) - std::max(newCornerX, firstCornerX));
            //if (xOverlap > 0 && ((xOverlap / std::min(newAffectedWidth, firstAffectedWidth)) > 0.90))
            if (xOverlap > 0 && ((xOverlap / firstAffectedWidth) > 0.75))
            {
              //Then want to merge
              allBoxes[q][0] = std::min(firstCornerX, newCornerX);
              allBoxes[q][2] = firstAffectedWidth + newAffectedWidth - xOverlap;
              allBoxes[q][3] += box_height; //only gets 1 box taller
              merged = true;
              break;
            }
          }
        }
        if (!merged)
        {
          temp.clear();
          temp.push_back(cornerX*box_width);
          temp.push_back(cornerY*box_height);
          temp.push_back(num_merged_boxes * box_width);
          temp.push_back(box_height);
          temp.push_back(input_width);
          temp.push_back(input_height);
          allBoxes.push_back(temp);
          //std::cout << std::endl << cornerX << " " << cornerY << " " << num_merged_boxes * box_width << " " << box_height << std::endl;
        }
        makingBox = false;
      }
    }
    else if (y != lasty && makingBox)
    {
      //End that box and push it back
      for (int q = 0; q < allBoxes.size(); q++)
      {
        firstCornerX = allBoxes[q][0];
        firstCornerY = allBoxes[q][1];
        firstAffectedWidth = allBoxes[q][2];
        firstAffectedHeight = allBoxes[q][3];
        newCornerX = cornerX * box_width;
        newCornerY = cornerY * box_height;
        newAffectedWidth = num_merged_boxes * box_width;
        if (newCornerY == (firstCornerY + firstAffectedHeight))
        {
          xOverlap = (std::min((firstCornerX + firstAffectedWidth), (newCornerX + newAffectedWidth)) - std::max(newCornerX, firstCornerX));
          //if (xOverlap > 0 && ((xOverlap / std::min(newAffectedWidth, firstAffectedWidth)) > 0.90))
          if (xOverlap > 0 && ((xOverlap / firstAffectedWidth) > 0.75))
          {
            //Then want to merge
            allBoxes[q][0] = std::min(firstCornerX, newCornerX);
            allBoxes[q][2] = firstAffectedWidth + newAffectedWidth - xOverlap;
            allBoxes[q][3] += box_height; //only gets 1 box taller
            merged = true;
            break;
          }
        }
      }
  
      if (!merged)
      {
        temp.clear();
        temp.push_back(cornerX*box_width);
        temp.push_back(cornerY*box_height);
        temp.push_back(num_merged_boxes * box_width);
        temp.push_back(box_height);
        temp.push_back(input_width);
        temp.push_back(input_height);
        allBoxes.push_back(temp);
      }
      //std::cout << cornerX << " " << cornerY << " " << num_merged_boxes * box_width << " " << box_height << std::endl;
      
      makingBox = false;
      if (updateInfo[i] == 1)
      {
        num_merged_boxes = 1;
        makingBox = true;
        cornerX = x;
        cornerY = y;
      }
    }
  
    else if (updateInfo[i] == 1)
    {
      num_merged_boxes = 1;
      makingBox = true;
      cornerX = x;
      cornerY = y;
    }

    update += updateInfo[i];
    lasty = y;
  }
  if (makingBox)
  {
    temp.clear();
    temp.push_back(cornerX*box_width);
    temp.push_back(cornerY*box_height);
    temp.push_back(num_merged_boxes * box_width);
    temp.push_back(box_height);
    temp.push_back(input_width);
    temp.push_back(input_height);
    allBoxes.push_back(temp);
    //std::cout << cornerX << " " << cornerY << " " << num_merged_boxes * box_width << " " << box_height << std::endl;
  }

  std::free(updateInfo);

  //std::cout << totalBoxes << " in the whole input \n";
  //std::cout << update << " need to be updated \n";
  //std::cout << allBoxes.size() << " are left after merge \n";

  int area = 0;
  int totalArea = 0;
  for (int k = 0; k < allBoxes.size(); k++)
  {
    //std::cout << allBoxes[k][0] << " " << allBoxes[k][1] << " " << allBoxes[k][2] << " " << allBoxes[k][3] << std::endl;
    area = allBoxes[k][2] * allBoxes[k][3];
    //Delete boxes of size 1, unlikely they are too relevant
    if (area == (box_width * box_height))
    {
      //Remove it if it's a small box
      allBoxes.erase(allBoxes.begin() + k, allBoxes.begin() + k + 1);
      k--;
    }
    else
    {
      totalArea += area;
      //std::cout << allBoxes[k][0] << " " << allBoxes[k][1] << " " << allBoxes[k][2] << " " << allBoxes[k][3] << std::endl;
    }

  }
  //std::cout << allBoxes.size() << " are left after removal of small boxes \n";
  float percentChange = (float(totalArea) / (input_width * input_height));
  std::cout << "Recompute area is " << percentChange * 100 << "% of the input at a Static Thresh of " << STATIC_THRESH * 100 << '\n';
  //exit(0);

  //Read in Recompute Threshold from file
  float RECOMPUTE_THRESH = 0.0;
  std::ifstream infile1("params/recompute.txt");
  if(!infile1) fatal_error("Cannot open recompute");
  std::string line1;
  infile1 >> line1;
  RECOMPUTE_THRESH = std::stof (line1);
  if (percentChange > RECOMPUTE_THRESH)
  {
    //We return 0 boxes to denote we should do full computation using original method
    //std::cout << "Should be doing original run instead of recompute \n";
    allBoxes.clear(); 
  }
  return allBoxes;
  //Here we want to return a vector of bounding boxes
  //Do the merge here?

}

void OpGraph::run_st(bool firstRun) {
  std::vector<std::string> ready_queue;
  ready_queue.reserve(info.size());
  std::vector<OpInfo*> now_ready;
  //float* newData;
  now_ready.reserve(info.size());
  outputs.reserve(info.size());
  unsigned queue_index = 0;

  //TensorContainer before;

  for(auto &p : info) {
    if(0 == p.second.unsatisfied) {
      ready_queue.push_back(p.first);
    }
  }

  auto t1 = std::chrono::high_resolution_clock::now();
  auto t2 = std::chrono::high_resolution_clock::now();
  int delta = 0;
  /*std::vector<int> first_box;
  std::vector<int> second_box;
  std::vector<int> third_box;
  std::vector<int> fourth_box;
  std::vector<int> bounding_box;*/

  //Give the first op the initial recomp input data
  std::string op_name = ready_queue[queue_index];
  bool change = false;

  //Should always save off old image for new bounding box computation
  if (firstRun)
  {
    oldInput = info[op_name].inputs[0];
  }

  if(!firstRun)
  {
    Tensor<float>& lastInput = *(oldInput.t_float);
    Tensor<float>& newInput = *(info[op_name].inputs[0].t_float);
    int input_height = newInput.dim(1);
    int input_width = newInput.dim(2);
    int input_depth = newInput.dim(3);
    const float* old_input_data = lastInput.ptr();
    const float* new_input_data = newInput.ptr();
    //int num_boxes_x = 96;
    //int num_boxes_y = 54;
    int num_boxes_x = 48;
    int num_boxes_y = 27;
    //int num_boxes_x = 32;
    //int num_boxes_y = 18;

    //Not going to use this for now, just set the sizes ourself
    newInput.bounding_boxes = compare(old_input_data, new_input_data, num_boxes_x, num_boxes_y, input_height, input_width, input_depth);
    //newInput.bounding_boxes.clear();
    //std::cout << "There are " << newInput.bounding_boxes.size() << " recompute boxes \n";
    //Make sure we have the current image for next time
    oldInput = info[op_name].inputs[0];

    //Order of args for recompute area is X, Y, width, height
    //
    //Group of People
    // 700 350 170 270
    //
    // By Trunk
    // 920 450 210 250
    //
    // Bottom Peeps
    // 670 800 180 280
    /*first_box.push_back(165);
    first_box.push_back(140);
    first_box.push_back(115);
    first_box.push_back(110);
    first_box.push_back(input_width);
    first_box.push_back(input_height);
    newInput.bounding_boxes.push_back(first_box);

    second_box.push_back(267);
    second_box.push_back(143);
    second_box.push_back(83);
    second_box.push_back(107);
    second_box.push_back(input_width);
    second_box.push_back(input_height);
    newInput.bounding_boxes.push_back(second_box);

    third_box.push_back(310);
    third_box.push_back(48);
    third_box.push_back(50);
    third_box.push_back(57);
    third_box.push_back(input_width);
    third_box.push_back(input_height);
    newInput.bounding_boxes.push_back(third_box); 

    fourth_box.push_back(0);
    fourth_box.push_back(0);
    fourth_box.push_back(75);
    fourth_box.push_back(60);
    fourth_box.push_back(input_width);
    fourth_box.push_back(input_height);
    newInput.bounding_boxes.push_back(fourth_box);*/
  }

  //While we still have ops to run
  //int counter = 0;
  while(ready_queue.size() > queue_index) {
    //Get the op name
    std::string op_name = ready_queue[queue_index];
    queue_index++;

    //if (!firstRun) {
    //  std::cout << "Starting " << op_name << std::endl;
    //}

    //Run its TimedCompute function in /src/ops/opp.cpp
    //t1 = std::chrono::high_resolution_clock::now();
    auto result = info[op_name].op->TimedCompute(info[op_name].inputs);
    //t2 = std::chrono::high_resolution_clock::now();
    //delta = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();
    //std::cout << "Took " << delta << " microseconds \n";

    if(info[op_name].outgoing.empty()) {
      for(TensorContainer &out : result) {
        outputs.push_back(std::make_pair(op_name, out));
      }
    }

    /*if(firstRun)
    {
      //check_data.insert( std::pair<std::string, TensorContainer> (op_name, result[0]) );
      float* oldData = TensorExtractor<float>::Get(result[0])->ptrInit();

      before = TensorContainer(TensorDType {DT_FLOAT}, result[0].shape());
      newData = TensorExtractor<float>::Get(before)->ptrInit();
      //std::memcpy(newData, oldData, result[0].elements()*sizeof(float));
      for(int i = 0; i < result[0].elements(); i++)
      {
        newData[i] = oldData[i];
      }
      check_data[op_name] = before;
     //std::cout << "After assignment " << check_data[op_name] << std::endl;
    }
    else
    {
      //std::cout << "OLD " << check_data[op_name] << std::endl;
      //std::cout << "NEW " << result[0] << std::endl;
      float error = get_error(result[0], check_data[op_name]);
      std::cout << "Error in " << op_name << ": " << error << std::endl;
      //std::cout  << op_name << ": " << error << std::endl;
      //if (error < 0.0001)
      //{
        //std::cout << "Error in " << op_name << ": " << error << std::endl;
        //std::cout << "Same? " << op_name << ": " << error << std::endl;
        //exit(0);
      //}
    }*/

    //Push that result along outgoing edges in the computation graph
    propagate_outputs(result, info[op_name].outgoing,
                      info[op_name].outgoing_control, now_ready);

    for(OpInfo *other : now_ready) {
      //std::cout << "now ready: " << other->op->name() << std::endl;
      ready_queue.push_back(other->op->name());
    }
  }
}

// round-robin assignment
void OpGraph::run_mt(bool firstRun) {
  //Going to use the TCs to know first or later run
  TensorContainer first = TensorContainer(1);
  TensorContainer recompute = TensorContainer(2);
  TensorContainer before;
  std::ifstream infile3 ("params/affected.txt");
  TensorContainer original_tc = load_tensor(infile3);

  //Original code
  const unsigned num_ops = info.size();
  std::vector<std::string> ready_queue(num_ops);
  std::vector<std::mutex> ready_lock(num_ops);
  outputs.reserve(num_ops);
  std::atomic<uint32_t> push_index(0), pop_index(0);
  for(auto &p : info) {
    if(0 == p.second.unsatisfied) {
      ready_queue[push_index++] = p.second.op->name();
      info[p.second.op->name()].inputs.push_back(original_tc);
    }
  }

  for(unsigned i = push_index.load(); i < num_ops; i++) {
    ready_lock[i].lock();
  }
  int inter = get_inter();
  #pragma omp parallel num_threads(inter)
  {
    std::vector<OpInfo*> now_ready;
    now_ready.reserve(info.size());
    unsigned local_index = pop_index++;
    while(local_index < num_ops) {
      ready_lock[local_index].lock();
      std::string op_name = ready_queue[local_index];

      //Need to tell the ops if we are in original or recompute mode
      if(firstRun)
      {
        info[op_name].inputs.push_back(first);
      }
      else
      {
        info[op_name].inputs.push_back(recompute);
        //for(TensorContainer &in : info[op_name].inputs) {
        //  std::cout << "Inputs: " << in << std::endl;
        //}
      }

#ifndef USE_CUDA
      auto result = info[op_name].op->TimedCompute(info[op_name].inputs);
#else
      auto result = info[op_name].op->TimedCompute(info[op_name].inputs, GPU);
#endif
      if(info[op_name].outgoing.empty()) {
        #pragma omp critical(output)
        {
          for(size_t i = 0; i < result.size(); i++) {
            outputs.push_back(std::make_pair(op_name, result[i]));
          }
        }
      }
      propagate_outputs(result, info[op_name].outgoing, info[op_name].outgoing_control, now_ready);
      for(OpInfo *other : now_ready) {
        unsigned ind = push_index++;
        ready_queue[ind] = other->op->name();
        ready_lock[ind].unlock();
      }
      ready_lock[local_index].unlock();
      local_index = pop_index++;
    }
  }
}

//continuation-first scheduling
void OpGraph::run_mt2() {
  const std::string term_str = "TERMINATE";
  const int inter = get_inter();
  const unsigned num_ops = info.size();
  outputs.reserve(num_ops);
  unsigned num_terminals = 0;
  std::vector<std::pair<std::mutex, std::string>> global_queue(num_ops);
  unsigned global_idx = 0;
  for(auto &p : info) {
    if(0 == p.second.unsatisfied) {
      global_queue[global_idx++].second = p.second.op->name();
    }
    if(0 == p.second.outgoing.size()) {
      num_terminals++;
    }
  }
  std::atomic<uint32_t> push_index(global_idx), pop_index(0), term_count(0);
  for(; global_idx < global_queue.size(); global_idx++) {
    global_queue[global_idx].first.lock();
  }
  #pragma omp parallel num_threads(inter)
  {
    OpInfo* previous_op = NULL;
    std::vector<OpInfo*> now_ready;
    now_ready.reserve(info.size());
    std::string next_op = "", op_name = "";
    while(true) {
      if(next_op.length() > 0) {
        op_name = next_op;
        next_op = "";
      } else {
        unsigned local_index = (pop_index++) % num_ops;;
        global_queue[local_index].first.lock();
        op_name = global_queue[local_index].second;
      }
      assert(op_name != "");
      if(op_name == term_str) break;
      auto &op_info = info[op_name];
      if(previous_op) {
        op_info.affinity_control.set_affinity(op_info.op->parallelism, previous_op->affinity_control.local_mask);
      } else {
        op_info.affinity_control.set_affinity(op_info.op->parallelism);
      }
      auto result = op_info.op->TimedCompute(op_info.inputs);
      previous_op = &op_info;
      op_info.affinity_control.clear_affinity();
      if(info[op_name].outgoing.empty()) {
        #pragma omp critical(output)
        {
          for(TensorContainer &out : result) {
            outputs.push_back(std::make_pair(op_name, out));
          }
        }
        if(num_terminals == ++term_count) {
          for(int i = 0; i < num_ops; i++) {
            global_queue[i].second = term_str;
            global_queue[i].first.unlock();
          }
        }
      }
      propagate_outputs(result, info[op_name].outgoing, info[op_name].outgoing_control, now_ready);
      for(OpInfo *other : now_ready) {
        if(next_op.length() == 0) {
          next_op = other->op->name();
        } else {
          unsigned ind = (push_index++) % num_ops;
          global_queue[ind].second = other->op->name();
          global_queue[ind].first.unlock();
        }
      }
      info[op_name].reset();
    }
  }
}

void OpGraph::bench_scalability(int max_threads) {
  std::vector<std::string> ready_queue;
  ready_queue.reserve(info.size());
  std::vector<OpInfo*> now_ready;
  now_ready.reserve(info.size());
  unsigned queue_index = 0;
  for(auto &p : info) {
    if(0 == p.second.unsatisfied) {
      ready_queue.push_back(p.first);
    }
  }
  std::cout << '*';
  for(int i = 1; i <= max_threads; i++) {
    std::cout << ' ' << i;
  }
  std::cout << '\n';
  std::cout.flush();
  while(ready_queue.size() > queue_index) {
    std::string op_name = ready_queue[queue_index];
    queue_index++;

    const auto &inputs = info[op_name].inputs;
    Op *op = info[op_name].op.get();
    std::cout << op_name;
    std::cout.flush();
    for(int i = 1; i <= max_threads; i++) {
      op->parallelism = i;
      for(int i = 0; i < 100; i++) {
        op->TimedCompute(inputs);
      }
      auto t1 = std::chrono::high_resolution_clock::now();
      for(int i = 0; i < 1000; i++) {
        op->TimedCompute(inputs);
      }
      auto t2 = std::chrono::high_resolution_clock::now();
      unsigned delta = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();
      std::cout << ' ' << delta / 1000.;
      std::cout.flush();
    }
    std::cout << '\n';
    std::cout.flush();

    auto result = info[op_name].op->TimedCompute(info[op_name].inputs);
    if(info[op_name].outgoing.empty()) {
      for(TensorContainer &out : result) {
        outputs.push_back(std::make_pair(op_name, out));
      }
    }
    propagate_outputs(result, info[op_name].outgoing, info[op_name].outgoing_control, now_ready);
    for(OpInfo *other : now_ready) {
      ready_queue.push_back(other->op->name());
    }
  }
}

