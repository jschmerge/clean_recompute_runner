#include <iostream>
#include <memory>
#include <cassert>
#include <stdlib.h>

#include "def.h"

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include "def.cu"
#endif

void localized_message(std::string msg, const char *file, int line) {
  std::cerr << "ERROR\t" << msg << " (from " << file << ":" << line << ")\n";
}

//template <typename T>
//const TensorDType DTypeMap<T>::type = DT_NONE;
template <>
const TensorDType DTypeMap<float>::type = DT_FLOAT;
template <>
const TensorDType DTypeMap<int>::type = DT_INT;
template <>
const TensorDType DTypeMap<long>::type = DT_LONG;
template <>
const TensorDType DTypeMap<std::string>::type = DT_STRING;

std::string dtype_string(TensorDType dt) {
  switch(dt) {
  case DT_FLOAT: return "DT_FLOAT";
  case DT_DOUBLE: return "DT_DOUBLE";
  case DT_INT: return "DT_INT";
  case DT_LONG: return "DT_LONG";
  case DT_STRING: return "DT_STRING";
  default: return "DT_NONE";
  }
}

#define make_case(str_val, dt_val) if(s == str_val) return dt_val
TensorDType string_dtype(std::string s) {
  make_case("DT_FLOAT", DT_FLOAT);
  make_case("DT_DOUBLE", DT_DOUBLE);
  make_case("DT_INT", DT_INT);
  make_case("DT_LONG", DT_LONG);
  make_case("DT_STRING", DT_STRING);
  return DT_NONE;
}
#undef make_case

size_t elements_count(std::vector<size_t> dims) {
  if(dims.size() == 0) return 0;
  size_t total = 1;
  for(size_t d : dims) total *= d;
  return total;
}

#ifndef USE_CUDA
template<typename T>
Tensor<T>::Tensor(const std::vector<size_t> &in_dimensions) : dimensions(in_dimensions), scalar_val(0) {
  num_elements = elements_count(in_dimensions);
}

template<typename T>
Tensor<T>::Tensor(const T val) : dimensions(0), scalar_val(val), num_elements(0) {
}

template<typename T>
Tensor<T>::~Tensor() {
  std::free(h_data);
}
#endif

template<typename T>
const std::vector<size_t> &Tensor<T>::shape() const {
  return dimensions;
}

template<typename T>
size_t Tensor<T>::dims() const {
  return dimensions.size();
}

template<typename T>
size_t Tensor<T>::dim(size_t idx) const {
  if(idx >= dimensions.size()) fatal_error("Non-existant dimension");
  return dimensions[idx];
}

template<typename T>
size_t Tensor<T>::elements() const {
  return num_elements;
}

#ifndef USE_CUDA
template<typename T>
T *Tensor<T>::ptrInit(int n) {
  assert(dimensions.size() > 0);
  assert(dimensions[0] > 0);
  if (!h_data) {
    h_data = (T *)std::malloc(num_elements * sizeof(T));
  }
  return h_data;
}

template<typename T>
const T *Tensor<T>::ptrShr(int n) {
  assert(dimensions.size() > 0);
  assert(dimensions[0] > 0);
  assert(h_data != nullptr);
  return h_data;
}

template<typename T>
const T *Tensor<T>::ptr(int n) {
  return ptrShr();
}
#endif

template<typename T>
inline T &Tensor<T>::get(const size_t idx) {
  assert(h_data != nullptr);
  assert(dimensions.size() == 1);
  assert(dimensions[0] > idx);
  return h_data[idx];
}

template<typename T>
inline const T &Tensor<T>::get(const size_t idx) const {
  assert(h_data != nullptr);
  assert(dimensions.size() == 1);
  assert(dimensions[0] > idx);
  return h_data[idx];
}

template<typename T>
inline T &Tensor<T>::get(const size_t idx, const size_t idx2) {
  assert(h_data != nullptr);
  assert(dimensions.size() == 2);
  assert(dimensions[0] > idx);
  assert(dimensions[1] > idx2);
  return h_data[idx * dimensions[1] + idx2];
}

template<typename T>
inline const T &Tensor<T>::get(const size_t idx, const size_t idx2) const {
  assert(h_data != nullptr);
  assert(dimensions.size() == 2);
  assert(dimensions[0] > idx);
  assert(dimensions[1] > idx2);
  return h_data[idx * dimensions[1] + idx2];
}

template<typename T>
inline const T &Tensor<T>::val() const {
  assert(dimensions.size() == 0);
  return scalar_val;
}

TensorContainer::TensorContainer() : dtype(DT_NONE), ttype(TT_NONE), 
      t_float(nullptr), t_int(nullptr), t_long(nullptr), t_string(nullptr) {}

#define init_or_null(target_type, field) field.reset((DTypeMap<T>::type == DTypeMap<target_type>::type) ? (Tensor<target_type>*)bare_ptr : NULL)
template<typename T>
TensorContainer::TensorContainer(const T val) : dtype(DTypeMap<T>::type), ttype(TT_SCALAR) {
  void *bare_ptr = new Tensor<T>(val);
  init_or_null(float, t_float);
  init_or_null(int, t_int);
  init_or_null(long, t_long);
  init_or_null(std::string, t_string);
}
#undef init_or_null

template<>
TensorContainer::TensorContainer(std::vector<TensorContainer> vec) {
  fatal_error("This should never be called, but gcc complains when it's not there");
}

#define init_or_null(target_type, field) if(dtype == DTypeMap<target_type>::type) field.reset(new Tensor<target_type>(dims))
TensorContainer::TensorContainer(TensorDType datatype, std::vector<size_t> dims) : 
      dtype(datatype), ttype(TT_NORMAL) {
  init_or_null(float, t_float);
  init_or_null(int, t_int);
  init_or_null(long, t_long);
  init_or_null(std::string, t_string);
}
#undef init_or_null

std::vector<size_t> TensorContainer::shape() const {
  switch(dtype) {
  case DT_FLOAT: return t_float->shape();
  case DT_INT: return t_int->shape();
  case DT_LONG: return t_long->shape();
  case DT_STRING: return t_string->shape();
  default: fatal_error("Unknown DType");
  }
}

size_t TensorContainer::elements() const {
  switch(dtype) {
  case DT_FLOAT: return t_float->elements();
  case DT_INT: return t_int->elements();
  case DT_LONG: return t_long->elements();
  case DT_STRING: return t_string->elements();
  default: fatal_error("Unknown DType");
  }
}

template<typename T>
Tensor<T> *TensorExtractor<T>::Get(TensorContainer &tc) { assert(false); return NULL; }

template<typename T>
const Tensor<T> *TensorExtractor<T>::Get(const TensorContainer &tc) { assert(false); return NULL; }

#define declare_get_tensor(tname, field) \
template<> Tensor<tname> *TensorExtractor<tname>::Get(TensorContainer &tc) { \
  if(DTypeMap<tname>::type == tc.dtype) return tc.field.get(); \
  else fatal_error("Attempt to get tensor of " + dtype_string(tc.dtype) + \
                   " as tensor of " + dtype_string(DTypeMap<tname>::type)); \
} \
template<> const Tensor<tname> *TensorExtractor<tname>::Get(const TensorContainer &tc) { \
  if(DTypeMap<tname>::type == tc.dtype) return tc.field.get(); \
  else fatal_error("Attempt to get tensor of " + dtype_string(tc.dtype) + \
                   " as tensor of " + dtype_string(DTypeMap<tname>::type)); \
} 
declare_get_tensor(float, t_float);
declare_get_tensor(int, t_int);
declare_get_tensor(long, t_long);
declare_get_tensor(std::string, t_string);
#undef declare_get_tensor

template<typename T>
void data_to_stream(std::ostream &dest, const T *ptr, size_t num) {
  for(size_t i = 0; i < num; i++) {
    dest << ptr[i] << '\n';;
  }
}
std::ostream& operator<<(std::ostream &stream, TensorContainer &tc) {
  stream << dtype_string(tc.dtype) << '\n';
  stream << tc.shape().size();
  for(int i = 0; i < tc.shape().size(); i++) {
    stream << ' ' << tc.shape()[i];
  }
  stream << '\n';
  #define make_case(datatype) if(tc.dtype == DTypeMap<datatype>::type) { \
    auto container = TensorExtractor<datatype>::Get(tc); \
    if(tc.ttype == TT_NORMAL) { \
      data_to_stream(stream, container->ptr(), container->elements()); \
    } else { \
      data_to_stream(stream, &(container->val()), 1); \
    } \
  }
  make_case(float);
  make_case(int);
  make_case(long);
  #undef make_case
  return stream;
}

template<typename T>
void data_from_stream(std::istream &src, T *ptr, size_t num) {
  for(size_t i = 0; i < num; i++) {
    src >> ptr[i];
  }
}
std::istream& operator<<(std::istream &stream, TensorContainer &tc) {
  return stream;
}

template class Tensor<float>;
template class Tensor<int>;
template class Tensor<long>;
template class Tensor<std::string>;

template TensorContainer::TensorContainer(float);
template TensorContainer::TensorContainer(int);
template TensorContainer::TensorContainer(long);
template TensorContainer::TensorContainer(std::string);

