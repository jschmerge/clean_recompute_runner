#include <iostream>
#include <fstream>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <omp.h>
#include <mkl.h>
#include <iomanip>
#include <string.h>

#include "ops/register.h"
#include "graph.h"
#include "load.h"

int main(int argc, char** argv) {
  RegisterOps();
  if(argc != 8) {
    std::cerr << "usage: ./run_graph <graph.def> \n";
    exit(1);
  }

  omp_set_nested(1);
  omp_set_dynamic(0);
  omp_set_max_active_levels(2);
  mkl_set_dynamic(0);

  std::cout << std::fixed << std::setprecision(10);
  //std::cout << "Loading the graph..." << std::endl;
  OpGraph op_graph = load_graph(argv[1]);
  //std::cout << "Done loading graph. \n \n";

  //Compute Method, Original, Strided, or Recompute
  bool original = (strcmp(argv[2], "original") == 0) ? true : false;
  bool strided = (strcmp(argv[2], "strided") == 0) ? true : false;
  bool recompute = (strcmp(argv[2], "recompute") == 0) ? true : false;

  //Total frames to consider, stride, and what frame to start on
  size_t totNumFrames = std::stoi(argv[3]) + 1;
  size_t stride = std::stoi(argv[4]);
  size_t startingFrame = std::stoi(argv[5]);
  std::string video = argv[6];
  std::string model = argv[7];

  //Control how run is done
  bool doOriginalRun;

  //Warm up the graph for better timing info
  doOriginalRun = true;
  if (!original)
  {
    for (int i = 0; i < 3; i++)
    {
      op_graph.run_st(doOriginalRun);
      //op_graph.run_mt(first);
      op_graph.reset();
    }
  }
  std::cout << "Done warming up the graph. \n \n";

  //Variables for below
  auto t1 = std::chrono::high_resolution_clock::now();
  auto t2 = std::chrono::high_resolution_clock::now();
  unsigned delta = 0;
  int image_num;
  std::string input_base_string = "models/" + video + "/frame";
  std::string output_org_base_string = model + "Results/ground";
  std::string output_str_base_string = model + "Results/str";
  std::string output_rec_base_string = model + "Results/rec";
  std::string in_filename;
  std::string out_filename;

  //Want to use original runner and compute full frame each time
  if (original || strided)
  {
    std::ofstream stream("orgTimings.txt");
    //This will just run the old runner as is
    doOriginalRun = true;
    image_num = startingFrame;
    while (image_num < startingFrame + totNumFrames)
    {
      //Make sure we get correct frame from that we are on and change to it
      //std::cout << "Doing recompute run on image " << image_num << "...\n";
      in_filename = input_base_string + std::to_string(image_num) + ".var";
      std::cout << "Current Image is " << in_filename << '\n';
      change_graph_input(op_graph, argv[1], in_filename, model);

      //Actually run the graph, can give timing info too
      t1 = std::chrono::high_resolution_clock::now();
      op_graph.run_st(doOriginalRun);
      t2 = std::chrono::high_resolution_clock::now();
      delta = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();

      //Put timing info in a final to generate summary from later
      //std::cout << delta << " microseconds for original run \n \n";
      stream << delta << '\n';
     
      //Only send outputs to file if we care about accuracy
      if (original)
      {
        out_filename = output_org_base_string + std::to_string(image_num) + ".txt";
      }
      else
      {
        out_filename = output_str_base_string + std::to_string(image_num) + ".txt";
      }
      op_graph.write_outputs_to_file(out_filename);

      //Reset graph so we are ready to do the next image
      op_graph.reset();

      //On original run, stride is always 1
      image_num += (original) ? 1 : stride;
    }
    stream.close();
  }

  if (recompute)
  {
    int images_processed = 0;
    //Runner needs to know to do original run
    doOriginalRun = true;
    std::ofstream recStream("recTimings.txt");

    //Should run on whatever starting image we are told
    in_filename = input_base_string + std::to_string(startingFrame) + ".var";
    std::cout << "Base image is " << in_filename << '\n';
    change_graph_input(op_graph, argv[1], in_filename, model);

    //We need a complete first run to get our intermediate results
    t1 = std::chrono::high_resolution_clock::now();
    op_graph.run_st(doOriginalRun);
    t2 = std::chrono::high_resolution_clock::now();
    delta = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();

    //Timing info for baseline run
    recStream << delta << '\n';

    //We should still output the first run
    out_filename = output_rec_base_string + std::to_string(startingFrame) + ".txt";
    op_graph.write_outputs_to_file(out_filename);

    op_graph.reset();

    //Run on recompute images, skip 0th image, it was done above
    doOriginalRun = false;
    image_num = startingFrame + stride;

    while (image_num < totNumFrames + startingFrame)
    {
      //Make sure we get correct frame from that we are on and change to it
      in_filename = input_base_string + std::to_string(image_num) + ".var";
      std::cout << "Current Image is " << in_filename << '\n';
      change_graph_input(op_graph, argv[1], in_filename, model);
      images_processed += 1;

      if (images_processed % 15 == 0)
      {
        doOriginalRun = true;
        //This code is resetting the ground truth anyway, can we pay a small overhead to
        //decide if we are meeting our accuracy requirement
      }

      //Actually run the graph, can give timing info too
      t1 = std::chrono::high_resolution_clock::now();
      op_graph.run_st(doOriginalRun);
      t2 = std::chrono::high_resolution_clock::now();
      delta = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();
      
      doOriginalRun = false;

      //Put timing info into file for analysis by script
      //std::cout << delta << " microseconds for second run \n \n";
      recStream << delta << '\n';

      //Put output info into file for analysis by script
      out_filename = output_rec_base_string + std::to_string(image_num) + ".txt";
      op_graph.write_outputs_to_file(out_filename);

      //Reset graph so we are ready to do the next image
      op_graph.reset();
      image_num += stride;
    }
    recStream.close();
  }
  return 0;
}
