#ifdef USE_CUDA

#include "op_batchnorm.hpp"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdio.h>

#define NSMS 20
#define MAX_BLOCKS 8
#define THREADS 256

__global__
void batchnorm_kernel(float * out, const float * x, const float * mean, const float * variance, const float * offset, const float * scale,
                   unsigned ntasks, unsigned tasksize, uint32_t mask, uint32_t * workIndex) {

  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if( !(mask & (1 << smid))) return;

  __shared__ size_t grabbedIndex;

  int tid = threadIdx.x;

  while(true) {
    if (tid == 0) {
      grabbedIndex = atomicAdd(workIndex, 1);
    } 
    
    __syncthreads();

    if (grabbedIndex >= ntasks) return;

    const size_t startIndex = grabbedIndex * tasksize;

    for (size_t i = 0; i < tasksize; i += THREADS) {
      size_t item = tid + i;
      if ( tid + i < tasksize ) {
        out[startIndex + item] = scale[item] * (x[startIndex + item] - mean[item]) / sqrt(variance[item] + .001) + offset[item];
      }
    }

    __syncthreads();

  }
}

TensorContainer BatchNormOp::GPUKernel(Tensor<float> &x, Tensor<float> &mean, Tensor<float> &variance,
                            Tensor<float> &offset, Tensor<float> &scale, uint32_t * workIndex) {
  const float * x_ptr = x.ptrShr(GPU);
  const float * mean_ptr = mean.ptrShr(GPU);
  const float * variance_ptr = variance.ptrShr(GPU);
  const float * offset_ptr = offset.ptrShr(GPU);
  const float * scale_ptr = scale.ptrShr(GPU);

  size_t elements = x.elements();
  size_t tasksize = x.dim(x.dims() - 1);
  size_t ntasks = elements / tasksize;

  TensorContainer out(DT_FLOAT, x.shape());
  float * out_ptr = out.t_float->ptrInit(GPU);

  uint32_t mask = GPUScheduler::schedule_sms(parallelism);

  batchnorm_kernel<<<NSMS*MAX_BLOCKS, THREADS, 0>>>(out_ptr, x_ptr, mean_ptr, variance_ptr, offset_ptr, scale_ptr,
                                                    ntasks, tasksize, mask, workIndex);
  cudaDeviceSynchronize();

  GPUScheduler::return_sms(mask);

  return out;
}

#endif

