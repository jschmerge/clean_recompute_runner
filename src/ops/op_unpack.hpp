#pragma once
#include <cstring>
#include <cassert>
#include <vector>

#include "common.h"

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#endif

class UnpackOp : public Op {
  using Op::Op;
public:
  std::vector<TensorContainer> Compute(const std::vector<TensorContainer>& args) override {
    assert(args.size() == 2);
    assert(args[0].ttype == TT_NORMAL);
    assert(args[0].dtype == DT_FLOAT);
    assert(args[1].ttype == TT_SCALAR);
    assert(args[1].dtype == DT_INT);
    return Kernel(*(args[0].t_float), args[1].t_int->val());
  }
  template<typename T>
  std::vector<TensorContainer> Kernel(Tensor<T> &A, const int axis) {
    std::cout << "In unpack \n";
    assert(axis < A.dims());
    const size_t num = A.dim(axis);
    std::vector<TensorContainer> outs;
    outs.reserve(num);
    std::vector<T*> ptrs;
    ptrs.reserve(num);
    std::vector<size_t> shape(A.shape());
    shape.erase(shape.begin() + axis);
    for(int i = 0; i < num; i++) {
      outs.push_back(TensorContainer(DTypeMap<T>::type, shape));
      ptrs.push_back(TensorExtractor<T>::Get(outs[i])->ptrInit());
    }
    size_t stride = 1;
    for(size_t i = axis; i < A.dims(); i++)
      stride *= A.dim(i);
    size_t contig = stride / num;
    size_t segments = A.elements() / stride;
    #pragma omp parallel for num_threads(parallelism)
    for(size_t s = 0; s < segments; s++) {
      for(int i = 0; i < num; i++) {
        memcpy(ptrs[i]+s*contig, A.ptr()+s*stride+i*contig, contig*sizeof(T));
      }
    }
    return outs;
  }

#ifdef USE_CUDA
  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer>& args, uint32_t * workIndex) override {
    assert(args.size() == 2);
    assert(args[0].ttype == TT_NORMAL);
    assert(args[0].dtype == DT_FLOAT);
    assert(args[1].ttype == TT_SCALAR);
    assert(args[1].dtype == DT_INT);
    return GPUKernel(*(args[0].t_float), args[1].t_int->val(), workIndex);
  }

  template<typename T>
  std::vector<TensorContainer> GPUKernel(Tensor<T> &A, const int axis, uint32_t * workIndex);
#endif

};

