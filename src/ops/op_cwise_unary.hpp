#pragma once
#include <cmath>
#include <cassert>
#include <vector>
#include <mutex>
#include <fstream>
#include <sstream>

#include "common.h"

std::mutex mtx;

/*    std::cout << "Corner (" << cornerX << "," << cornerY << ")\n"; \
    std::cout << "Size is " << affectedWidth << " " << affectedHeight << std::endl; \
    std::cout << "Layer info is " << outputWidth << " " << outputHeight << std::endl; \*/

#define IMPLEMENT_OP(OP_NAME, ARG1, EXPRESSION) \
class OP_NAME : public UnaryOp { \
public: \
  OP_NAME(std::string n, int t) : UnaryOp(n, t) {} \
  \
  make_unary_compute(float); \
  make_unary_compute(int); \
  make_unary_compute(long); \
\
  inline std::pair<size_t, size_t> offset_stride(size_t dim, int tid, int threads) { \
    size_t stride = dim / threads; \
    size_t offset = stride * tid; \
    size_t residual = dim - (stride * threads); \
    if(tid < residual) return std::make_pair(offset + tid, stride + 1); \
    else return std::make_pair(offset + residual, stride); \
  } \
\
  template<typename T> \
  std::vector<TensorContainer> Kernel(Tensor<T> &A) { \
    int mode = 1; \
    int cornerX = 0; \
    int cornerY = 0; \
    int affectedWidth = 0; \
    int affectedHeight = 0; \
    int outputWidth = 0; \
    int outputHeight = 0; \
    int num_boxes = A.bounding_boxes.size(); \
    \
    /* Just read in dynamic thresh from file here */ \
    float DYNAMIC_THRESH = 0.0; \
    std::ifstream infile("params/dynamic.txt"); \
    if(!infile) fatal_error("Cannot open dynamic"); \
    std::string line; \
    infile >> line; \
    DYNAMIC_THRESH = std::stof (line); \
    \
    if (num_boxes > 0) { \
      mode = 2; \
    } \
    std::vector<std::vector<int> > tempBoxes; \
    std::vector<int> bb; \
		TensorContainer out(DTypeMap<T>::type, A.shape()); \
		T *arr_out; \
		size_t elements = A.elements(); \
    if (mode == 1) { \
		  const T *arr_A = A.ptr(); \
		  arr_out = TensorExtractor<T>::Get(out)->ptrInit(); \
      run(arr_A, arr_out, elements); \
      op_output = out; \
    } \
    else if (mode == 2) { \
		  const T *arr_A = A.ptr(); \
		  arr_out = TensorExtractor<T>::Get(op_output)->ptrInit(); \
      for (int q = 0; q < num_boxes; q++) { \
        cornerX = A.bounding_boxes[q][0]; \
        cornerY = A.bounding_boxes[q][1]; \
        affectedWidth = A.bounding_boxes[q][2]; \
        affectedHeight = A.bounding_boxes[q][3]; \
        outputWidth = A.bounding_boxes[q][4]; \
        outputHeight = A.bounding_boxes[q][5]; \
        recompRun(arr_A, arr_out, elements, cornerX, cornerY, affectedWidth, affectedHeight, outputWidth, outputHeight, DYNAMIC_THRESH); \
        /* std::cout << "AFTER " << cornerX << " " << cornerY << " " << affectedWidth << " " << affectedHeight << std::endl; */ \
        /* if (affectedWidth == 0 || affectedHeight == 0) */ \
        /* { continue; } */ \
        bb.clear(); \
        bb.push_back(cornerX); \
        bb.push_back(cornerY); \
        bb.push_back(affectedWidth); \
        bb.push_back(affectedHeight); \
        bb.push_back(outputWidth); \
        bb.push_back(outputHeight); \
        tempBoxes.push_back(bb); \
      } \
    out = op_output; \
    }\
    if (mode == 2) { \
      Tensor<float>& out_data = *(out.t_float); \
      out_data.bounding_boxes.clear(); \
      for (int z = 0; z < tempBoxes.size(); z++) { \
        out_data.bounding_boxes.push_back(tempBoxes[z]); \
      }\
    } \
    /* std::cout << "After dynamic recompute, there are " << tempBoxes.size() << " boxes left \n"; */ \
    \
		return {out}; \
	} \
\
  template<typename T> \
  void run(const T *A, T *out, size_t elements) { \
    /*int zero_count = 0;*/ \
    _Pragma("omp parallel for num_threads(parallelism) ") \
		for(size_t i = 0; i < elements; i++) { \
      T ARG1 = A[i]; \
			out[i] = EXPRESSION; \
		} \
	} \
\
  template<typename T> \
  void recompRun(const T *A, T *out, size_t elements, int &cornerX, int &cornerY, int &affectedWidth, int &affectedHeight, int outputWidth, int outputHeight, float DYNAMIC_THRESH) { \
    int depth = elements / (outputWidth*outputHeight); \
    float thresh = DYNAMIC_THRESH * depth; \
    int first_patch = (cornerY * outputWidth) + cornerX; \
    std::vector<int> minX (parallelism, affectedWidth); \
    std::vector<int> maxX (parallelism, 0); \
    std::vector<int> minY (parallelism, affectedHeight); \
    std::vector<int> maxY (parallelism, 0); \
    _Pragma("omp parallel for num_threads(parallelism) ") \
    for (size_t parallel_index = 0; parallel_index < parallelism; parallel_index++) { \
      std::pair<size_t, size_t> work = offset_stride(affectedWidth*affectedHeight, parallel_index, parallelism); \
      const size_t patch_index_start = work.first; \
      const size_t patch_index_end = work.first + work.second; \
      float diff = 0.0; \
      double temp = 0.0; \
      float old_val = 0; \
      float new_val = 0; \
      int row = 0; \
      int col = 0; \
      size_t patch_index = 0; \
      int spot = 0; \
\
      for(int i = patch_index_start; i < patch_index_end; i++) { \
        row = i/affectedWidth; \
        col = i%affectedWidth + first_patch; \
        patch_index = (col+(outputWidth*row)); \
        col -= first_patch; \
        diff = 0; \
        temp = 0; \
        for(int k = 0; k < depth; k++) { \
          spot = (patch_index*depth) + k; \
          old_val = out[spot]; \
          T ARG1 = A[spot]; \
          out[spot] = EXPRESSION; \
          new_val = out[spot]; \
          temp = std::max(new_val, old_val); \
          /* Avoid a divide by zero */ \
          if (temp == 0.0) { continue; } \
          diff += (std::abs( new_val - old_val))/temp; \
        } \
        if (diff > thresh) { \
          if (col < minX[parallel_index]) { minX[parallel_index] = col; } \
          if (col > maxX[parallel_index]) { maxX[parallel_index] = col; } \
          if (row < minY[parallel_index]) { minY[parallel_index] = row; } \
          if (row > maxY[parallel_index]) { maxY[parallel_index] = row; } \
        } \
      }\
    }\
    int smallestX = minX[0]; \
    int smallestY = minY[0]; \
    int largestX = maxX[0]; \
    int largestY = maxY[0]; \
    for (int i = 1; i < parallelism; i++) { \
      if (minX[i] < smallestX) { smallestX = minX[i]; } \
      if (minY[i] < smallestY) { smallestY = minY[i]; } \
      if (maxX[i] > largestX) { largestX = maxX[i]; } \
      if (maxY[i] > largestY) { largestY = maxY[i]; } \
    } \
    int width = (largestX-smallestX) + 1; \
    int height = (largestY-smallestY) + 1; \
    if (width < 0) { width = 0;} \
    if (height < 0) { height = 0;} \
    if (height < affectedHeight) { affectedHeight = height; } \
    if (width < affectedWidth) { affectedWidth = width; } \
    cornerX += smallestX; \
    cornerY += smallestY; \
\
  }\
};

IMPLEMENT_OP(SigmoidOp, x, 1.0/(1+exp(-x)));
IMPLEMENT_OP(TanhOp, x, tanh(x));
IMPLEMENT_OP(RsqrtOp, x, 1.0/sqrt(x));
IMPLEMENT_OP(ReluOp, x, x > 0 ? x : 0);

#undef IMPLEMENT_OP

/*	
 *
    for (int i = 1; i < parallelism; i++) { \
      if (minX[i] < smallestX) smallestX = minX[i]; \
      if (minY[i] < smallestY) smallestY = minY[i]; \
      if (maxX[i] > largestX) largestX = maxX[i]; \
      if (maxY[i] > largestY) largestY = maxY[i]; \
    } \
    int width = (largestX-smallestX) + 1; \
    int height = (largestY-smallestY) + 1; \
    if (width < 0) { width = 0;} \
    if (height < 0) { height = 0;} \
    if (height < affectedHeight) { affectedHeight = height; } \
    if (width < affectedWidth) { affectedWidth = width; } \
    cornerX = minX; \
    cornerY = minY; \
 *
 *
 *
 *	for(size_t i = 0; i < elements; i++) { \
			if (out[i] < 0.00000000000000001) { \
			  zero_count++; \
      } \
		} \
    std::cout << "Ratio is " << (float)zero_count / elements << std::endl;
    int big_zero_count = 0; \
    std::cout << "Ratio in recompute area is " << ((float)zero_count / (affectedWidth*affectedHeight*depth))*100 << "% of zero elements" << std::endl; 
    std::vector<int> newArea = minCompare(old_area, new_area, depth, affectedWidth, affectedHeight);

        if (diff > 0) { \
          mtx.lock(); \
          if (j < minX) { minX = j; } \
          if (j > maxX) { maxX = j; } \
          if (i < minY) { minY = i; } \
          if (i > maxY) { maxY = i; } \
          mtx.unlock(); \
        } \
      } \
    } \
    int width = (maxX-minX) + 1; \
    int height = (maxY-minY) + 1; \
    if (width < 0) { width = 0;} \
    if (height < 0) { height = 0;} \
    if (height < affectedHeight) { affectedHeight = height; } \
    if (width < affectedWidth) { affectedWidth = width; } \
    cornerX = minX; \
    cornerY = minY; \

    printf("OLD corner (%d, %d) and is %d by %d \n", cornerX, cornerY, affectedWidth, affectedHeight ); \
    printf("NEW corner (%d, %d) and is %d by %d \n \n", cornerX, cornerY, affectedWidth, affectedHeight ); \
    */
