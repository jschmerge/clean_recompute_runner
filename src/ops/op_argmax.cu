#ifdef USE_CUDA

#include "op_argmax.hpp"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdio.h>

#define NSMS 20
#define MAX_BLOCKS 64
#define THREADS 32

template<typename T>
__global__
void argmax_kernel(T * out, const T * in, unsigned length, unsigned ntasks, uint32_t mask, uint32_t * workIndex) {

  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if( !(mask & (1 << smid))) return;

  __shared__ size_t grabbedIndex;

  int tid = threadIdx.x;

  if (blockIdx.x == 0 && tid == 0) {
    printf("GPU Pointer value: %lx\n", in);
  }

  if (blockIdx.x == 0) {
    printf("Arr_GPU[%d]: %d\n", tid, in[tid]);
  }

  while(true) {
    if (tid == 0) {
      grabbedIndex = atomicAdd(workIndex, 1);
    }

    __syncthreads();

    if (grabbedIndex >= ntasks) return;

    const T * rstart = in + grabbedIndex * length;

    __shared__ T reduce[2 * THREADS];
    
    if (tid < length) {
      reduce[tid] = rstart[tid];
      reduce[tid + THREADS] = tid;
    }

    for (int i = 0; i < length; i += THREADS) {
      if (tid + i < length) {
        T temp = rstart[i + tid];
        if (temp > reduce[tid + THREADS]) {
          reduce[tid] = temp;
          reduce[tid + THREADS] = tid + i;
        }
      }
    }

    __syncthreads();

    #pragma unroll 
    for (int i = THREADS; i > 0; i /= 2) {
      if (tid < i) {
        if (reduce[tid + i] > reduce[tid])
          reduce[tid] = reduce[tid + i];
          reduce[tid + THREADS] = reduce[tid + i + THREADS];
      }
    }
    
    if (tid == 0) out[grabbedIndex] = reduce[THREADS];
  }
}

template<typename T>
TensorContainer ArgMaxOp::GPUKernel(Tensor<T> &A, Tensor<T> &B, uint32_t * workIndex) {
  TensorContainer out(DTypeMap<T>::type, {A.dim(0)});
  T* arr_out = TensorExtractor<T>::Get(out)->ptrInit(GPU);

  const T* arr_A = A.ptrShr(GPU);

  const T* arr_CPU = A.ptrShr(CPU);
  for (size_t i = 0; i < 32; i++) {
    std::cout << "Arr_CPU[" << i << "]: " << arr_CPU[i] << "\n";
  }

  unsigned ntasks = A.dim(0);
  unsigned length = A.dim(1);

  uint32_t mask = GPUScheduler::schedule_sms(parallelism);
  argmax_kernel<T><<<NSMS*MAX_BLOCKS, THREADS, 0>>>(arr_out, arr_A, length, ntasks, mask, workIndex);
  cudaDeviceSynchronize();

  GPUScheduler::return_sms(mask);

  return out;
}

template TensorContainer ArgMaxOp::GPUKernel<float>(Tensor<float>&, Tensor<float>&, uint32_t *);
template TensorContainer ArgMaxOp::GPUKernel<int>(Tensor<int>&, Tensor<int>&, uint32_t *);
template TensorContainer ArgMaxOp::GPUKernel<long>(Tensor<long>&, Tensor<long>&, uint32_t *);

#endif
