#pragma once
#include <cstring>
#include <algorithm>
#include <cassert>
#include <vector>

#include "common.h"

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#endif

class ConcatOp : public Op {
  using Op::Op;
public:
  std::vector<TensorContainer> Compute(const std::vector<TensorContainer>& args) override {
    assert(args.size() >= 3);
    Tensor<float>& A = *(args[1].t_float);
    int cornerX = 0;
    int cornerY = 0;
    int affectedWidth = 0;
    int affectedHeight = 0;
    //int outputWidth = 0;
    //int outputHeight = 0;
    int mode = 1;
    std::vector<std::vector<int> > boxes;
    std::vector<int> tempBox;
    int num_boxes = A.bounding_boxes.size();
    for (int i = 2; i < args.size(); i++) {
      Tensor<float>& B = *(args[i].t_float);
      if (B.bounding_boxes.size() > num_boxes) {
        num_boxes = B.bounding_boxes.size();
      }
    }

    if (num_boxes > 0) {
      mode = 2;
      for (int q = 0; q < num_boxes; q++) {
        tempBox.clear();
        if (A.bounding_boxes.size() > q) {
          cornerX = A.bounding_boxes[q][0];
          cornerY = A.bounding_boxes[q][1];
          affectedWidth = A.bounding_boxes[q][2];
          affectedHeight = A.bounding_boxes[q][3];
          //outputWidth = A.bounding_boxes[q][4];
          //outputHeight = A.bounding_boxes[q][5];
        }
        else {
          cornerX = 0;
          cornerY = 0;
          affectedWidth = 0;
          affectedHeight = 0;
        }
        //std::cout << cornerX << " " << cornerY << " " << affectedWidth << " " << affectedHeight << std::endl;
        //std::cout << "Layer size is " << outputWidth << " " << outputHeight << std::endl;
        //std::cout << "Corner (" << in_data1.cornerX << "," << in_data1.cornerY << ")\n";

        for (int i = 2; i < args.size(); i++)
        {
          Tensor<float>& B = *(args[i].t_float);
          if (B.bounding_boxes.size() > q && B.bounding_boxes[q][0] < cornerX) {
            cornerX = B.bounding_boxes[q][0];
          }

          if (B.bounding_boxes.size() > q && B.bounding_boxes[q][1] < cornerY) {
            cornerY = B.bounding_boxes[q][1];
          }

          if (B.bounding_boxes.size() > q && B.bounding_boxes[q][2] > affectedWidth) {
            affectedWidth = B.bounding_boxes[q][2];
          }

          if (B.bounding_boxes.size() > q && B.bounding_boxes[q][3] > affectedHeight) {
            affectedHeight = B.bounding_boxes[q][3];
          }
          //std::cout << cornerX << " " << cornerY << " " << affectedWidth << " " << affectedHeight << std::endl;
        }

        /*if (affectedHeight + cornerY > outputHeight)
        {
          affectedHeight = outputHeight - cornerY;
        }

        if (affectedWidth + cornerX > outputWidth)
        {
          affectedWidth = outputWidth - cornerX;
        }
        std::cout << "corner " << cornerX << " " << cornerY << " size " << affectedWidth << " " << affectedHeight << std::endl;*/
        tempBox.push_back(cornerX);
        tempBox.push_back(cornerY);
        tempBox.push_back(affectedWidth);
        tempBox.push_back(affectedHeight);
        //tempBox.push_back(outputWidth);
        //tempBox.push_back(outputHeight);
        boxes.push_back(tempBox);
      }
    }

    //std::cout << "Final Corner (" << cornerX << "," << cornerY << ")\n";
    //std::cout << "Final Size is " << affectedWidth << " " << affectedHeight << std::endl;

    size_t offset = 0;
    int axis;
    if(args[0].ttype == TT_SCALAR) {
      assert(args[0].dtype == DT_INT);
      offset = 1;
      axis = args[0].t_int->val();
    } else {
      assert(args[args.size() - 1].ttype == TT_SCALAR);
      assert(args[args.size() - 1].dtype == DT_INT);
      axis = args[args.size() - 1].t_int->val();
    }
    const size_t n_tensors = args.size() - 1;
    size_t n_dims = args[offset].shape().size();
    TensorDType dtype = args[offset].dtype;
    assert(axis > 0 && axis < n_dims);
    for(size_t k = 1; k < n_tensors; k++) {
      assert(args[k+offset].shape().size() == n_dims);
      assert(args[k+offset].dtype == dtype);
    }
    for(size_t i = 0; i < n_dims; i++) {
      if(i == axis) continue;
      size_t dim = args[offset].shape()[i];
      for(size_t k = 1; k < n_tensors; k++) 
      {
        assert(args[k+offset].shape()[i] == dim);
      }
    }
    std::vector<size_t> shape(args[offset].shape());
    for(size_t k = 1; k < n_tensors; k++) {
      shape[axis] += args[k+offset].shape()[axis];
    }
    std::vector<size_t> strides(n_tensors, 1);
    for(size_t i = axis; i < n_dims; i++) {
      for(size_t k = 0; k < n_tensors; k++) {
        strides[k] *= args[k+offset].shape()[i];
      }
    }

    TensorContainer tc(dtype, shape);
    //#define make_case(_dtype, _type, _field) if(dtype == _dtype) 
    if(args[offset].dtype == DT_FLOAT) {
      size_t n_strides = args[offset].t_float->elements() / strides[0];
      std::vector<const float*> ptrs(n_tensors);
      for(size_t i = 0; i < n_tensors; i++) {
        assert(strides[i] * n_strides == args[i+offset].t_float->elements());
        ptrs[i] = args[i+offset].t_float->ptr();
      }
      Kernel(tc.t_float->ptrInit(), ptrs, strides, n_strides);
      if (mode == 2) {
        Tensor<float>& out_data = *(tc.t_float);
        out_data.bounding_boxes.clear();
        for (int q = 0; q < boxes.size(); q++) { 
          boxes[q].push_back(out_data.dim(0));
          boxes[q].push_back(out_data.dim(1));
          out_data.bounding_boxes.push_back(boxes[q]);
        }
      }
      return { tc };
    } else {
      fatal_error("Unknown dtype");
      return std::vector<TensorContainer>();
    }
  }
  template<typename T>
  void Kernel(T *p_out, std::vector<const T*> &ptrs, 
              std::vector<size_t> &strides, size_t n_strides) {
    size_t sum_strides = strides[0];
    std::vector<size_t> prefix_strides(strides.size(), 0);
    for(size_t i = 1; i < strides.size(); i++) {
      sum_strides += strides[i];
      prefix_strides[i] = prefix_strides[i - 1] + strides[i - 1];
    }
    //T *prev_end = p_out;
    #pragma omp parallel for num_threads(parallelism)
    for(size_t seg = 0; seg < n_strides; seg++) {
      for(size_t tid = 0; tid < ptrs.size(); tid++) {
        //assert(prev_end == p_out + seg * sum_strides + prefix_strides[tid]);
        memcpy(p_out + seg * sum_strides + prefix_strides[tid],
               ptrs[tid] + seg * strides[tid], strides[tid] * sizeof(T));
        //prev_end += strides[tid];
      }
    }
  }

#ifdef USE_CUDA
  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer>& args, uint32_t * workIndex) override {
    assert(args.size() >= 3);
    size_t offset = 0;
    int axis;
    if(args[0].ttype == TT_SCALAR) {
      assert(args[0].dtype == DT_INT);
      offset = 1;
      axis = args[0].t_int->val();
    } else {
      assert(args[args.size() - 1].ttype == TT_SCALAR);
      assert(args[args.size() - 1].dtype == DT_INT);
      axis = args[args.size() - 1].t_int->val();
    }
    const size_t n_tensors = args.size() - 1;
    size_t n_dims = args[offset].shape().size();
    TensorDType dtype = args[offset].dtype;
    assert(axis > 0 && axis < n_dims);
    for(size_t k = 1; k < n_tensors; k++) {
      assert(args[k+offset].shape().size() == n_dims);
      assert(args[k+offset].dtype == dtype);
    }
    for(size_t i = 0; i < n_dims; i++) {
      if(i == axis) continue;
      size_t dim = args[offset].shape()[i];
      for(size_t k = 1; k < n_tensors; k++) {
        assert(args[k+offset].shape()[i] == dim);
      }
    }
    std::vector<size_t> shape(args[offset].shape());
    for(size_t k = 1; k < n_tensors; k++) {
      shape[axis] += args[k+offset].shape()[axis];
    }
    std::vector<size_t> strides(n_tensors, 1);
    for(size_t i = axis; i < n_dims; i++) {
      for(size_t k = 0; k < n_tensors; k++) {
        strides[k] *= args[k+offset].shape()[i];
      }
    }
    TensorContainer tc(dtype, shape);
    //#define make_case(_dtype, _type, _field) if(dtype == _dtype) 
    if(args[offset].dtype == DT_FLOAT) {
      size_t n_strides = args[offset].t_float->elements() / strides[0];
      std::vector<const float*> ptrs(n_tensors);
      for(size_t i = 0; i < n_tensors; i++) {
        assert(strides[i] * n_strides == args[i+offset].t_float->elements());
        ptrs[i] = args[i+offset].t_float->ptrShr(GPU);
      }
      GPUKernel(tc.t_float->ptrInit(GPU), ptrs, strides, n_strides, workIndex);
      return { tc };
    } else {
      fatal_error("Unknown dtype");
      return std::vector<TensorContainer>();
    }
  }
  
  template<typename T>
  void GPUKernel(T *p_out, std::vector<const T*> &ptrs, 
              std::vector<size_t> &strides, size_t n_strides, uint32_t * workIndex);
#endif

};

