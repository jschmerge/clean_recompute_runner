#pragma once

#include <algorithm>
#include <cassert>

#include "common.h"

/*    std::cout << "Corner (" << cornerX << "," << cornerY << ")\n"; \
    std::cout << "Size is " << affectedWidth << " " << affectedHeight << std::endl; \
    std::cout << "Layer info is " << outputWidth << " " << outputHeight << std::endl; \ */

#define IMPLEMENT_OP(OP_NAME, ARG_1, ARG_2, EXPRESSION) \
class OP_NAME : public BinaryOp { \
public: \
  OP_NAME(std::string s, int i) : BinaryOp(s, i) {}\
  make_binary_compute(float); \
  make_binary_compute(int); \
  make_binary_compute(long); \
\
  template<typename T> \
  std::vector<TensorContainer> Kernel(Tensor<T> &A, Tensor<T> &B) { \
    /* Are we even doing recompute here right now? */ \
    int mode = 1; \
    std::vector<std::vector<int> > boxes; \
    std::vector<int> tempBox; \
    if (A.bounding_boxes.size() > 0) { \
      int cornerX = 0; \
      int cornerY = 0; \
      int affectedWidth = 0; \
      int affectedHeight = 0; \
      int outputWidth = 0; \
      int outputHeight = 0; \
      mode = 2; \
      /* std::cout << "MODE " << mode << " " << A.bounding_boxes.size() << " " << B.bounding_boxes.size() << std::endl; */ \
      for (int q = 0; q < A.bounding_boxes.size(); q++) { \
        tempBox.clear(); \
        cornerX = A.bounding_boxes[q][0]; \
        cornerY = A.bounding_boxes[q][1]; \
        affectedWidth = A.bounding_boxes[q][2]; \
        affectedHeight = A.bounding_boxes[q][3]; \
        outputWidth = A.bounding_boxes[q][4]; \
        outputHeight = A.bounding_boxes[q][5]; \
        if (B.bounding_boxes.size() > 0 && B.bounding_boxes.size() < q) { \
          if (B.bounding_boxes[q][0] < cornerX) { \
            cornerX = B.bounding_boxes[q][0]; \
          } \
          if (B.bounding_boxes[q][1] < cornerY) { \
            cornerY = B.bounding_boxes[q][1]; \
          } \
          if (B.bounding_boxes[q][2] < affectedWidth) { \
            affectedWidth = B.bounding_boxes[q][2]; \
          } \
          if (B.bounding_boxes[q][3] < affectedHeight) { \
            affectedHeight = B.bounding_boxes[q][3]; \
          } \
        } \
        /* std::cout << cornerX << " " << cornerY << " " << affectedWidth << " " << affectedHeight << " " << outputWidth << " " << outputHeight << std::endl; */ \
        tempBox.push_back(cornerX); \
        tempBox.push_back(cornerY); \
        tempBox.push_back(affectedWidth); \
        tempBox.push_back(affectedHeight); \
        tempBox.push_back(outputWidth); \
        tempBox.push_back(outputHeight); \
        boxes.push_back(tempBox); \
      } \
    } \
\
    const size_t A_size = A.elements(), B_size = B.elements(); \
    assert((A_size >= B_size) == (A.dims() >= B.dims())); \
    std::vector<size_t> shape(B.shape()); \
    if(A_size > B_size) shape = A.shape(); \
    if(A.dims() > B.dims()) shape = A.shape(); \
		TensorContainer out(DTypeMap<T>::type, shape); \
    T *out_ptr = TensorExtractor<T>::Get(out)->ptrInit(); \
    if(A_size == B_size) { \
      const T *A_ptr = A.ptr(), *B_ptr = B.ptr(); \
      run_fast(A_ptr, B_ptr, out_ptr, A_size); \
    } else if(A.dims() == 0) { \
      const T *B_ptr = B.ptr(); \
      run_scalar(A.val(), B_ptr, out_ptr, B_size); \
    } else if(B.dims() == 0) { \
      const T *A_ptr = A.ptr(); \
      run_scalar(A_ptr, B.val(), out_ptr, A_size); \
    } else { \
      const T *A_ptr = A.ptr(), *B_ptr = B.ptr(); \
      const size_t high = std::max(A_size, B_size); \
      const size_t low = std::min(A_size, B_size); \
      if(high % low != 0) { \
        fatal_error("Dimension Mismatch in " + name()); \
      } \
      const size_t slices = high / low; \
      if(slices > low) { \
        _Pragma("omp parallel for num_threads(parallelism) ") \
        for(size_t i = 0; i < slices; i++) { \
          run_single(A_ptr + (i * low) % A_size, B_ptr + (i * low) % B_size, out_ptr + i * low, low); \
        } \
      } else { \
        for(size_t i = 0; i < slices; i++) { \
          run_fast(A_ptr + (i * low) % A_size, B_ptr + (i * low) % B_size, out_ptr + i * low, low); \
        } \
      } \
    } \
    if (mode == 2) { \
      Tensor<float>& out_data = *(out.t_float); \
      out_data.bounding_boxes.clear(); \
      for (int q = 0; q < boxes.size(); q++) { \
        /* std::cout << boxes[q][0] << " " << boxes[q][1] << " " << boxes[q][2] << " " << boxes[q][3] << std::endl; */ \
        out_data.bounding_boxes.push_back(boxes[q]); \
      } \
    } \
    \
		return {out}; \
	} \
\
  template<typename T> \
  void run_fast(const T *A, const T *B, T *out, size_t elements) { \
    _Pragma("omp parallel for num_threads(parallelism) ") \
		for(size_t i = 0; i < elements; i++) { \
      T ARG_1 = A[i]; \
      T ARG_2 = B[i]; \
			out[i] = EXPRESSION; \
		} \
	} \
  template<typename T> \
  void run_scalar(const T *A, const T val, T *out, size_t elements) { \
    const T ARG_2 = val; \
    _Pragma("omp parallel for num_threads(parallelism) ") \
		for(size_t i = 0; i < elements; i++) { \
      T ARG_1 = A[i]; \
			out[i] = EXPRESSION; \
		} \
  } \
  template<typename T> \
  void run_scalar(const T val, const T *B, T *out, size_t elements) { \
    const T ARG_1 = val; \
    _Pragma("omp parallel for num_threads(parallelism) ") \
		for(size_t i = 0; i < elements; i++) { \
      T ARG_2 = B[i]; \
			out[i] = EXPRESSION; \
		} \
  } \
\
  template<typename T> \
  void run_single(const T *A, const T *B, T *out, size_t elements) { \
		for(size_t i = 0; i < elements; i++) { \
      T ARG_1 = A[i]; \
      T ARG_2 = B[i]; \
			out[i] = EXPRESSION; \
		} \
	} \
};

IMPLEMENT_OP(SubOp, x, y, x-y);
IMPLEMENT_OP(AddOp, x, y, x+y);
IMPLEMENT_OP(MulOp, x, y, x*y);
IMPLEMENT_OP(EqualOp, x, y, x==y);

#undef IMPLEMENT_OP

/*  template<typename T> \
  void recompRun(const T *A, T *out, size_t elements, int cornerX, int cornerY, int affectedWidth, int affectedHeight, int outputWidth, int outputHeight) { \
    int depth = elements / (outputWidth*outputHeight); \
    _Pragma("omp parallel for num_threads(parallelism) ") \
    for(int i = cornerY; i < cornerY+affectedHeight; i++) { \
      for(int j = cornerX; j <  cornerX+affectedWidth; j++) { \
        for(int k = 0; k < depth; k++) { \
          int spot = (i*outputWidth*depth)+(j*depth)+k; \
          T ARG1 = A[spot]; \
          out[spot] = EXPRESSION; \
        } \
      } \
    } \
	} \*/
        /*if (A.bounding_boxes[q].size() != 6) { \
          std::cout << "Ruh Roh " << A.bounding_boxes.size() << std::endl; \
          exit(0); \
        } \ */
