#pragma once

#include "common.h"

class ShapeOp : public Op {
  using Op::Op;
public:
  std::vector<TensorContainer> Compute(const std::vector<TensorContainer>& args) override {
    std::cout << "In Shape \n";
    assert(args.size() == 1);
    std::vector<size_t> shape = args[0].shape();
    TensorContainer tc(DT_INT, { shape.size() });
    int *ptr = tc.t_int->ptrInit();
    for(size_t i = 0; i < shape.size(); i++) {
      ptr[i] = shape[i];
    }
    return { tc };
  }

#ifdef USE_CUDA
  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer>& args, uint32_t * workIndex) override {
    return Compute(args);
  }
#endif

};

