#pragma once
#include <cstring>
#include <cassert>
#include <vector>

#include "common.h"

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#endif

class SplitOp : public Op {
  using Op::Op;
public:
  std::vector<TensorContainer> Compute(const std::vector<TensorContainer>& args) override {
    assert(args.size() == 3);
    assert(args[0].ttype == TT_SCALAR);
    assert(args[0].dtype == DT_INT); //axis
    assert(args[1].dtype == DT_FLOAT); //data
    assert(args[2].dtype == DT_INT); //num_split
    assert(args[2].ttype == TT_SCALAR);
    return Kernel(args[0].t_int->val(), *(args[1].t_float), args[2].t_int->val());
  }
  template<typename T>
  std::vector<TensorContainer> Kernel(const int axis, Tensor<T> &B, const int num_split) {
    std::cout << "In split \n";
		assert(axis < B.dims());
    assert(B.dim(axis) % num_split == 0);
    std::vector<TensorContainer> outs;
    outs.reserve(num_split);
    std::vector<T*> ptrs;
    ptrs.reserve(num_split);
    std::vector<size_t> shape(B.shape());
    shape[axis] /= num_split;
    for(int i = 0; i < num_split; i++) {
      outs.push_back(TensorContainer(DTypeMap<T>::type, shape));
      ptrs.push_back(TensorExtractor<T>::Get(outs[i])->ptrInit());
    }
    size_t stride = 1;
    for(size_t i = axis; i < B.dims(); i++)
      stride *= B.dim(i);
    size_t contig = stride / num_split;
    size_t segments = B.elements() / stride;
    #pragma omp parallel for num_threads(parallelism)
    for(size_t s = 0; s < segments; s++) {
      for(int i = 0; i < num_split; i++) {
        memcpy(ptrs[i]+s*contig, B.ptr()+s*stride+i*contig, contig*sizeof(T));
      }
    }
    return outs;
  }

#ifdef USE_CUDA
  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer>& args, uint32_t * workIndex) override {
    assert(args.size() == 3);
    assert(args[0].ttype == TT_SCALAR);
    assert(args[0].dtype == DT_INT);
    assert(args[1].dtype == DT_FLOAT);
    assert(args[2].ttype == TT_SCALAR);
    assert(args[2].dtype == DT_INT);
    return GPUKernel(args[0].t_int->val(), *(args[1].t_float), args[2].t_int->val(), workIndex);
  }

  template<typename T>
  std::vector<TensorContainer> GPUKernel(const int axis, Tensor<T> &B, const int num_split, uint32_t * workIndex);
#endif

};

class SplitVOp : public Op {
  using Op::Op;
public:
  std::vector<TensorContainer> Compute(const std::vector<TensorContainer>& args) override {
    assert(args.size() == 3);
    assert(args[0].dtype == DT_FLOAT); //data
    assert(args[1].shape().size() == 1); //split_dims
    assert(args[2].dtype == DT_INT); //axis
    assert(args[2].ttype == TT_SCALAR);
    //std::cout << "axis=" << args[2].t_int->val() << "\nnsplit=" << args[1].shape()[0] << '\n';
    return Kernel(args[2].t_int->val(), *(args[0].t_float), args[1].shape()[0], args[1].t_int->ptr());
  }
  template<typename T>
  std::vector<TensorContainer> Kernel(const int axis, Tensor<T> &B, const int num_split, const int *split_dims) {
    assert(axis < B.dims());
    std::vector<TensorContainer> outs;
    outs.reserve(num_split);
    std::vector<T*> ptrs;
    ptrs.reserve(num_split);
    size_t axis_sum = 0;
    size_t base_stride = 1;
    for(int i = axis+1; i < B.dims(); i++) {
      base_stride *= B.shape()[i];
    }
    for(int i = 0; i < num_split; i++) {
      std::vector<size_t> shape(B.shape());
      shape[axis] = split_dims[i];
      axis_sum += split_dims[i];
      outs.push_back(TensorContainer(DTypeMap<T>::type, shape));
      ptrs.push_back(TensorExtractor<T>::Get(outs[i])->ptrInit());
    }
    size_t sum_strides = base_stride * axis_sum;
    std::vector<size_t> prefix_strides(num_split, 0);
    for(int i = 1; i < num_split; i++) {
      prefix_strides[i] = prefix_strides[i - 1] + base_stride * split_dims[i - 1];
    }
    size_t segments = B.elements() / sum_strides;
    const T *p_in = B.ptr();
    #pragma omp parallel for num_threads(parallelism)
    for(size_t seg = 0; seg < segments; seg++) {
      for(int tid = 0; tid < num_split; tid++) {
        memcpy(ptrs[tid] + seg * split_dims[tid] * base_stride, 
               p_in + seg * sum_strides + prefix_strides[tid], 
               split_dims[tid] * base_stride * sizeof(T));
      }
    }
    return outs;
  }

#ifdef USE_CUDA
  make_gpucompute_unimplemented();
#endif

};

