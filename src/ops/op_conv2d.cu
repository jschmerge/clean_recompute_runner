#ifdef USE_CUDA

#include "op_conv2d.hpp"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdio.h>
#include <string>

#define BLOCK_SIZE 16
#define NSMS 20
#define MAX_BLOCKS 8


__global__
void conv2d_kernel(float * C, const float * A, const float * B, size_t m, size_t n, size_t k, 
                   size_t ntasks, size_t tasksize, size_t wtask,  uint32_t mask, uint32_t * workIndex) {

  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if ( !(mask & (1 << smid))) return;

  __shared__ size_t grabbedIndex;

  int tx = threadIdx.x;
  int ty = threadIdx.y;

  while(true) {
    if (tx == 0 && ty == 0) {
      grabbedIndex = atomicAdd(workIndex, 1);
    }

    __syncthreads();

    if (grabbedIndex >= ntasks) return;

    int by = grabbedIndex / wtask;
    int bx = grabbedIndex % wtask;

    int aBegin = n * BLOCK_SIZE * by;
    int aEnd = aBegin + n - 1;
    int aStep = BLOCK_SIZE;

    int bBegin = BLOCK_SIZE * bx;
    int bStep = BLOCK_SIZE * k;
    
    float Csub = 0;

    for (int a = aBegin, b = bBegin; 
         a <= aEnd;
         a += aStep, b += bStep) {

      __shared__ float As[BLOCK_SIZE][BLOCK_SIZE];
      __shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];

      if ((a % n + tx) < n && (a / m) + ty < m) { 
        As[ty][tx] = A[a + n * ty + tx];
      } else {
        As[ty][tx] = 0;
      }
      
      if ((b % k + tx) < k && (b / n) + ty < n) {
        Bs[ty][tx] = B[b + k * ty + tx];
      } else {
        Bs[ty][tx] = 0;
      }

      __syncthreads();
      
      #pragma unroll
      for (int l = 0; l < BLOCK_SIZE; l++) {
        Csub += As[ty][l] * Bs[l][tx];
      }
    
      __syncthreads(); 
    }
  
    int c = k * BLOCK_SIZE * by + BLOCK_SIZE * bx;

    if( (c % k + tx) < k && (c / m + ty) < m) {
      C[c + k * ty + tx] = Csub;
    }
  }
}

TensorContainer Conv2DOp::GPUKernel(Tensor<float> &input, Tensor<float> &filters,
                                    Tensor<int> &strides, Tensor<std::string> &padding, uint32_t * workIndex) {
  std::cout << "Entering convolution........";
  assert(input.dims() == 4);
  assert(filters.dims() == 4);
  assert(strides.dims() == 1);
  assert(strides.dim(0) == 4);
  
  assert(input.dim(3) == filters.dim(2));

  const float* input_data = input.ptrShr(GPU);
  int input_batches = input.dim(0);
  int input_height = input.dim(1);
  int input_width = input.dim(2);
  int input_depth = input.dim(3);

  const float* filter_data = filters.ptrShr(GPU);
  int filter_height = filters.dim(0);
  int filter_width = filters.dim(1);
  int filter_count = filters.dim(3);

  int stride_rows = strides.get(1);
  int stride_cols = strides.get(2);

  std::string padding_str = padding.val();

  int output_height;
  int output_width;
  if (!padding_str.compare("SAME")) {
    output_height = (input_height + stride_rows - 1) / stride_rows;
    output_width  = (input_width + stride_cols - 1) / stride_cols;
  } else {
    output_height = (input_height - filter_height + stride_rows) / stride_rows;
    output_width  = (input_width - filter_width + stride_cols) / stride_cols;
  }
  
  assert(output_height > 0);
  assert(output_width > 0);
  
  TensorContainer output_container(DT_FLOAT, {input_batches, output_height, output_width, filter_count});
  float* output_data = TensorExtractor<float>::Get(output_container)->ptrInit(GPU); 
  
  if ((input_batches <= 0) || (input_width <= 0) || (input_height <= 0) ||
      (input_depth <= 0)) {
    fatal_error("Invalid input dimensions for convolution");
  }
  if ((filter_width <= 0) || (filter_height <= 0) || (filter_count <= 0)) {
    fatal_error("Invalid filter dimensions for convolution");
  }
  if ((output_width <= 0) || (output_height <= 0)) {
    fatal_error("Invalid output dimensions for 2d convolution");
  }

  // We can just use a GEMM if the im2col is the identity operator, e.g., if
  // the kernel is 1x1 or the input data and filter have same height/width.
  if (filter_height == 1 && filter_width == 1 && stride_rows == 1 &&
      stride_cols == 1) {
    // The kernel is 1x1.
    size_t m = input_batches * input_height * input_width;
    size_t n = input_depth;
    size_t k = filter_count;
    
    parallelism = 20;
    uint32_t mask = GPUScheduler::schedule_sms(parallelism);
    size_t ntasks = ((m + BLOCK_SIZE - 1) / BLOCK_SIZE) * ((k + BLOCK_SIZE -1 ) / BLOCK_SIZE);
    size_t tasksize = (ntasks + parallelism - 1) / parallelism;
    size_t wtask = (k + BLOCK_SIZE - 1) / BLOCK_SIZE;

    dim3 mm_block(BLOCK_SIZE, BLOCK_SIZE);
    conv2d_kernel<<<NSMS * MAX_BLOCKS, mm_block, 0>>>(output_data, input_data, filter_data,
                                                    m, n, k,
                                                    ntasks, tasksize, wtask, mask, workIndex);
    cudaDeviceSynchronize();
    return output_container;
  } else if (filter_height == input_height && filter_width == input_width &&
             !padding_str.compare("VALID")) {
    // The input data and filter have the same height/width.
    size_t m = input_batches;
    size_t n = input_height * input_width * input_depth;
    size_t k = filter_count;
    
    parallelism = 20;
    uint32_t mask = GPUScheduler::schedule_sms(parallelism);
    size_t ntasks = ((m + BLOCK_SIZE - 1) / BLOCK_SIZE) * ((k + BLOCK_SIZE -1 ) / BLOCK_SIZE);
    size_t tasksize = (ntasks + parallelism - 1) / parallelism;
    size_t wtask = (k + BLOCK_SIZE - 1) / BLOCK_SIZE;

    dim3 mm_block(BLOCK_SIZE, BLOCK_SIZE);
    conv2d_kernel<<<NSMS * MAX_BLOCKS, mm_block, 0>>>(output_data, input_data, filter_data,
                                                    m, n, k,
                                                    ntasks, tasksize, wtask, mask, workIndex);
    cudaDeviceSynchronize();
    return output_container;
  }

  // These calculations define how the patches will be positioned within the
  // input image. The actual definitions are quite complex, and rely on the
  // previously-calculated output size.
  int filter_left_offset;
  int filter_top_offset;
  if (!padding_str.compare("VALID")) {
    filter_left_offset =
        ((output_width - 1) * stride_cols + filter_width - input_width + 1) /
        2;
    filter_top_offset = ((output_height - 1) * stride_rows + filter_height -
                         input_height + 1) /
                        2;
  } else {
    filter_left_offset =
        ((output_width - 1) * stride_cols + filter_width - input_width) / 2;
    filter_top_offset =
        ((output_height - 1) * stride_rows + filter_height - input_height) /
        2;
  }

  // The im2col buffer has # of patches rows, and # of filters cols.
  // It's laid out like this, in row major order in memory:
  //        < filter value count >
  //   ^   +---------------------+
  // patch |                     |
  // count |                     |
  //   v   +---------------------+
  // Each patch row contains a filter_width x filter_height patch of the
  // input, with the depth channel as the most contiguous in memory, followed
  // by the width, then the height. This is the standard memory order in the
  // image world if it helps to visualize it.
  const size_t filter_value_count = filter_width * filter_height * input_depth;
  const size_t patch_count = (input_batches * output_height * output_width);
  const float patches_per_parallel = patch_count / parallelism;

  float * im2col_buffer;
  cudaMalloc((void **)&im2col_buffer, patch_count * filter_value_count * sizeof(float));

  #pragma omp parallel for num_threads(parallelism) 
  for (size_t parallel_index = 0; parallel_index < parallelism; ++parallel_index) {
    const size_t patch_index_start = parallel_index * patches_per_parallel;
    const size_t patch_index_end =
        std::min((size_t)((parallel_index + 1) * patches_per_parallel), patch_count);
    for (size_t patch_index = patch_index_start; patch_index < patch_index_end;
         ++patch_index) {
      const size_t batch = patch_index / (output_height * output_width);
      const size_t out_y = (patch_index / output_width) % output_height;
      const size_t out_x = patch_index % output_width;
      const float* input_batch_start =
          input_data + (batch * input_height * input_width * input_depth);
      const int in_y_origin = (out_y * stride_rows) - filter_top_offset;
      const int in_x_origin = (out_x * stride_cols) - filter_left_offset;
      float* im2col_patch_start =
          im2col_buffer + (patch_index * filter_value_count);
      for (int filter_y = 0; filter_y < filter_height; ++filter_y) {
        const int in_y = in_y_origin + filter_y;
        float* im2col_row_start =
            im2col_patch_start + (filter_y * filter_width * input_depth);
        // If we're off the top or the bottom of the input, fill the
        // whole row with zeroes.
        if ((in_y < 0) || (in_y >= input_height)) {
          float* im2col_row_end =
              im2col_row_start + (filter_width * input_depth);
          std::fill(im2col_row_start, im2col_row_end, 0.0);
        } else {
          // What we're doing here is trying to copy and fill the im2col
          // buffer as efficiently as possible, using functions to set or
          // duplicate values en masse. We know we don't have to worry about
          // vertical edges because we dealt with that case above, so we
          // just need to handle filters that overlap the left or right
          // edges. Here's what that looks like:
          //
          // < left_zero_count > < center_copy_count > < right_zero_count >
          // +------------------+---------------------+--------------------+
          // |     (filter)     |       (image)       |      (filter)      |
          // +------------------+---------------------+--------------------+
          // in_x_origin        0                 input_width       in_x_end
          //
          // In reality it's unlikely that a filter patch will be wider
          // than an input, but this shows all the edge cases.
          // We use std::fill() to set the left and right sections to zeroes
          // and std::copy() to copy over the input data for the center.
          const int in_x_end = in_x_origin + filter_width;
          const int left_zero_count = std::max(0, 0 - in_x_origin);
          const int right_zero_count = std::max(0, in_x_end - input_width);
          const int center_copy_count =
              filter_width - (left_zero_count + right_zero_count);
          if (left_zero_count > 0) {
            float* im2col_left_start = im2col_row_start;
            cudaMemsetAsync(im2col_left_start, 0, left_zero_count * input_depth * sizeof(float));
          }
          if (center_copy_count > 0) {
            const float* input_row_start =
                input_batch_start + (in_y * input_width * input_depth) +
                (std::max(0, in_x_origin) * input_depth);
            float* im2col_center_start =
                im2col_row_start + (left_zero_count * input_depth);
            cudaMemcpyAsync(im2col_center_start, input_row_start, center_copy_count * input_depth * sizeof(float), cudaMemcpyDeviceToDevice);
          }
          if (right_zero_count > 0) {
            float* im2col_right_start =
                im2col_row_start +
                ((left_zero_count + center_copy_count) * input_depth);
            cudaMemsetAsync(im2col_right_start, 0, right_zero_count * input_depth * sizeof(float));
          }
        }
      }
    }
  }
  
  size_t m = patch_count;
  size_t n = filter_value_count;
  size_t k = filter_count;
  
  parallelism = 20;
  uint32_t mask = GPUScheduler::schedule_sms(parallelism);
  size_t ntasks = ((m + BLOCK_SIZE - 1) / BLOCK_SIZE) * ((k + BLOCK_SIZE -1 ) / BLOCK_SIZE);
  size_t tasksize = (ntasks + parallelism - 1) / parallelism;
  size_t wtask = (k + BLOCK_SIZE - 1) / BLOCK_SIZE;

  dim3 mm_block(BLOCK_SIZE, BLOCK_SIZE);
  conv2d_kernel<<<NSMS * MAX_BLOCKS, mm_block, 0>>>(output_data, im2col_buffer, filter_data,
                                                  m, n, k,
                                                  ntasks, tasksize, wtask, mask, workIndex);
  cudaDeviceSynchronize();
    
  cudaFree(im2col_buffer);
  
  GPUScheduler::return_sms(mask);

  std::cout << "Ending\n.";
  
  return output_container;
}

#endif
