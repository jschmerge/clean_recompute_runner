#ifdef USE_CUDA

#include "op_gather.hpp"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdio.h>

#define NSMS 20
#define MAX_BLOCKS 64
#define THREADS 32

template<typename T>
__global__
void launch_gather( float * out_data, const float * in_data, const T * indices, const size_t stride, const size_t ntasks, uint32_t mask, uint32_t * workIndex) {
  
  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if( !(mask & (1 << smid))) return;

  __shared__ size_t grabbedIndex;

  int tid = threadIdx.x;

  while(true) {
    if (tid == 0) {
      grabbedIndex = atomicAdd(workIndex, 1);
    }

    __syncthreads();

    if (grabbedIndex >= ntasks) return;

    for (size_t i = 0; i < stride; i += THREADS) {
      size_t offset = tid + i * THREADS;
      out_data[stride * grabbedIndex + offset] = in_data[ indices[grabbedIndex] * stride + offset];
    }
  }
}


template<typename T1, typename T2>
TensorContainer GatherOp::GPUKernel(Tensor<T1> &A, Tensor<T2> &B, uint32_t * workIndex) {
  std::vector<size_t> out_dims(B.shape());
  out_dims.insert(out_dims.end(), A.shape().begin()+1, A.shape().end());
  size_t stride = 1;
  for(size_t i = 1; i < A.shape().size(); i++) {
    stride *= A.shape()[i];
  }
  TensorContainer out(DTypeMap<T1>::type, out_dims);
  Tensor<T1> *t_out = TensorExtractor<T1>::Get(out);
  T1 *p_out = t_out->ptrInit(GPU);
  const T1 *p_A = A.ptrShr(GPU);
  const T2 *p_B = B.ptrShr(GPU);
  
  uint32_t mask = GPUScheduler::schedule_sms(parallelism);
  const size_t ntasks = B.elements();
  
  launch_gather<T2><<<MAX_BLOCKS * NSMS, THREADS, 0>>>(p_out, p_A, p_B, stride, ntasks, mask, workIndex);

  cudaDeviceSynchronize();

  GPUScheduler::return_sms(mask);

  return out;
}

template TensorContainer GatherOp::GPUKernel<float, int>(Tensor<float> &A, Tensor<int> &B, uint32_t * workIndex);
template TensorContainer GatherOp::GPUKernel<float, long>(Tensor<float> &A, Tensor<long> &B, uint32_t * workIndex);

#endif
