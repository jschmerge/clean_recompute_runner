#pragma once

#include "common.h"
        
class FusedBatchNormOp : public Op {
  using Op::Op;
public:
  std::vector<TensorContainer> Compute(const std::vector<TensorContainer>& args) override {
    //auto d = TensorExtractor<float>::Get(args[0]);
    //std::cout << "Corner (" << d->cornerX << "," << d->cornerY << ")\n";
    //std::cout << "Size is " << d->affectedWidth << " " << d->affectedHeight << std::endl;
    //std::cout << "Layer info is " << d->layerWidth << " " << d->layerHeight << std::endl;
    return { args[0] };
  }
#ifdef USE_CUDA
  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer>& args, uint32_t * workIndex) override {
    return { args[0] };
  }
#endif
};
