#ifdef USE_CUDA

#include "op_concat.hpp"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#include <iostream>

#define NSMS 20
#define MAX_BLOCKS 256
#define THREADS 8

template <typename T>
__global__
void launch_concat(T * out, 
                   T ** ptrs, size_t num_ptrs,
                   size_t * strides, size_t sum_strides,
                   size_t ntasks, uint32_t mask, uint32_t * workIndex) {
  
  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if( !(mask & (1 << smid))) return;

  __shared__ size_t grabbedIndex;

  int tid = threadIdx.x;

  while(true) {

    if (tid == 0) {
      grabbedIndex = atomicAdd(workIndex, 1);
    }

    __syncthreads();

    if (grabbedIndex >= ntasks) return;

    for (size_t j = 0; j < sum_strides; j += THREADS) {
      size_t offset = tid + j;
      size_t ptrOffset = strides[0];
      size_t ptrIndex = 0;
     
      if ( offset + strides[ptrIndex + 1] < sum_strides ) {
        while ( offset > ptrOffset) {
          ptrIndex++;
          ptrOffset += strides[ptrIndex];
        }

        ptrOffset -= strides[ptrIndex];

        out[grabbedIndex * sum_strides + offset] = ptrs[ptrIndex][grabbedIndex * strides[ptrIndex] + offset - ptrOffset];
      }

    }
  }
}


template<typename T>
void ConcatOp::GPUKernel(T *p_out, std::vector<const T*> &ptrs, 
                         std::vector<size_t> &strides, size_t n_strides, uint32_t * workIndex) {
  size_t sum_strides = strides[0];

  T** d_ptrs;
  cudaMalloc((void **)d_ptrs, sizeof(T*) * ptrs.size());
  
  size_t * d_strides;
  cudaMalloc((void **)&d_strides, sizeof(size_t) * strides.size());

  cudaMemcpyAsync((void *)d_ptrs, (void *)&ptrs[0], sizeof(T) * ptrs.size(), cudaMemcpyDefault);
  cudaMemcpyAsync((void *)d_strides, (void *)&strides[0], sizeof(size_t) * strides.size(), cudaMemcpyDefault);

  size_t num_ptrs = ptrs.size();
  size_t ntasks = n_strides;
  uint32_t mask = GPUScheduler::schedule_sms(parallelism);
  
  launch_concat<<<MAX_BLOCKS*NSMS, THREADS, 0>>>(p_out,
                                                 d_ptrs, num_ptrs,
                                                 d_strides, sum_strides,
                                                 ntasks, mask, workIndex);

  GPUScheduler::return_sms(mask);
  
  cudaDeviceSynchronize();

}

template void ConcatOp::GPUKernel<float>(float * p_out, std::vector<const float *> &ptrs, 
                                         std::vector<size_t> &strides, size_t n_strides, uint32_t * workIndex);
#endif 
