#ifdef USE_CUDA

#include "op_bias_add.hpp"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdio.h>

#define NSMS 20
#define BLOCK_SIZE 256
#define MAX_BLOCKS 8
#define K 1

template<typename T>
__global__
void bias_add_op(const T * A, const T * B, T * out, size_t elements, size_t colSize, uint32_t mask, uint32_t * workIndex) {

  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if ( !(mask & ( 1 << smid))) return;

  int tid = threadIdx.x;
  int bDim = blockDim.x;

  __shared__ size_t grabbedIndex;
  while (true) {
    if ( tid == 0 ) {
      grabbedIndex = atomicAdd(workIndex, bDim * K);
    }

    __syncthreads();
    if (grabbedIndex >= elements) return;

    #pragma unroll
    for (size_t i = 0; i < K; i++) {
      if ( grabbedIndex + tid + bDim * i < elements) {

        out[grabbedIndex + tid + bDim * i] = A[grabbedIndex + tid + bDim * i] + B[(grabbedIndex + tid + bDim * i) % colSize];

      } else {
        return;
      }
    }
  }
}

template<typename T>
TensorContainer BiasAddOp::GPUKernel(Tensor<T> &A, Tensor<T> &B, uint32_t * workIndex) {
  if(A.dims() != 2 || B.dims() != 1 || A.dim(1) != B.dim(0)) {
    for(int i = 0; i < A.dims(); i++) std::cout << A.dim(i) << ' ';
    std::cout << '\n';
    for(int i = 0; i < B.dims(); i++) std::cout << B.dim(i) << ' ';
    std::cout << '\n';
    fatal_error("Dimension Mismatch in " + name());
  }
  TensorContainer out(DTypeMap<T>::type, A.shape());
  const T *arr_A = A.ptrShr(GPU);
  const T *arr_B = B.ptrShr(GPU);
  T *arr_out = TensorExtractor<T>::Get(out)->ptrInit(GPU);
  const size_t elements = A.elements(), cols = A.dim(1);
  
  uint32_t mask = GPUScheduler::schedule_sms(parallelism);
  bias_add_op<T><<<NSMS*MAX_BLOCKS, BLOCK_SIZE, 0>>>(arr_A, arr_B, arr_out, elements, cols, mask, workIndex);

  GPUScheduler::return_sms(mask);
  cudaDeviceSynchronize();
  return out;
}

template TensorContainer BiasAddOp::GPUKernel<float>(Tensor<float>& A, Tensor<float>& B, uint32_t * workIndex);
template TensorContainer BiasAddOp::GPUKernel<int>(Tensor<int>& A, Tensor<int>& B, uint32_t * workIndex);
template TensorContainer BiasAddOp::GPUKernel<long>(Tensor<long>& A, Tensor<long>& B, uint32_t * workIndex);

#endif


