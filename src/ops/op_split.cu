#ifdef USE_CUDA

#include "op_split.hpp"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

#define NSMS 20
#define MAX_BLOCKS 8
#define THREADS 256

template<typename T>
__global__
void launch_split(T ** ptrs, int num_split, const T * in_data, size_t contig, size_t ntasks, uint32_t mask, uint32_t * workIndex) {

  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if( !(mask & (1 << smid))) return;

  __shared__ size_t grabbedIndex;

  int tid = threadIdx.x;

  while(true) {
    if (tid == 0) {
      grabbedIndex = atomicAdd(workIndex, 1);
    }

    __syncthreads();

    if (grabbedIndex >= ntasks) return;

    for (size_t i = 0; i < contig * num_split; i += THREADS) {
      size_t offset = tid + i;
      ptrs[offset / contig][grabbedIndex * contig + offset % contig] = in_data[grabbedIndex * contig * num_split + offset];
    }
  }
}


template<typename T>
std::vector<TensorContainer> SplitOp::GPUKernel(const int axis, Tensor<T> &B, const int num_split, uint32_t * workIndex) {
  assert(axis < B.dims());
  assert(B.dim(axis) % num_split == 0);

  std::vector<TensorContainer> outs;
  outs.reserve(num_split);

  std::vector<T*> ptrs;
  ptrs.reserve(num_split);
  
  std::vector<size_t> shape(B.shape());
  shape[axis] /= num_split;
  for(int i = 0; i < num_split; i++) {
    outs.push_back(TensorContainer(DTypeMap<T>::type, shape));
    ptrs.push_back(TensorExtractor<T>::Get(outs[i])->ptrInit(GPU));
  }
  
  size_t stride = 1;
  for(size_t i = axis; i < B.dims(); i++)
    stride *= B.dim(i);
  size_t contig = stride / num_split;
  size_t segments = B.elements() / stride;
  const T * b_data = B.ptrShr(GPU);
  
  T ** d_ptrs;
  cudaMalloc((void **)d_ptrs, sizeof(T *) * num_split);
  cudaMemcpyAsync((void *)d_ptrs, (void *)&ptrs, sizeof(T *) * num_split, cudaMemcpyDefault);
  
  size_t ntasks = segments;
  uint32_t mask = GPUScheduler::schedule_sms(parallelism);

  launch_split<T><<<NSMS*MAX_BLOCKS, THREADS, 0>>>(d_ptrs, num_split,
                                                   b_data, contig,
                                                   ntasks, mask, workIndex);
  
  GPUScheduler::return_sms(mask);
  cudaDeviceSynchronize();
  return outs;
}

template std::vector<TensorContainer> SplitOp::GPUKernel<float>(const int axis, Tensor<float> &B, const int num_split, uint32_t * workIndex);

#endif

