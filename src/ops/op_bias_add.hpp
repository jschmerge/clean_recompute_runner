#pragma once
#include "common.h"

class BiasAddOp : public BinaryOp {
  using BinaryOp::BinaryOp;
public:
  make_binary_compute(float);
  make_binary_compute(int);
  make_binary_compute(long);
  template<typename T>
  std::vector<TensorContainer> Kernel(Tensor<T> &A, Tensor<T> &B) {
    //Get my info out of the input tensor
    int cornerX = 0;
    int cornerY = 0;
    int affectedWidth = 0;
    int affectedHeight = 0;
    int outputWidth = 0;
    int outputHeight = 0;
    std::vector<std::vector<int> > boxes;
    std::vector<int> tempBox;

    int mode = 1;
    if (B.bounding_boxes.size() > 0)
    {
      std::cout << "Bias add has more than 0 B boxes " << B.bounding_boxes.size() << std::endl;
      exit(0);
    }
    if(A.bounding_boxes.size() > 0)
    {
      mode = 2;
    }
    //std::cout << "MODE " << mode << " " << A.bounding_boxes.size() << " " << B.bounding_boxes.size() << std::endl;

    if (mode == 2) {
      for (int q = 0; q < A.bounding_boxes.size(); q++) {
        tempBox.clear();
        cornerX = A.bounding_boxes[q][0];
        cornerY = A.bounding_boxes[q][1];
        affectedWidth = A.bounding_boxes[q][2];
        affectedHeight = A.bounding_boxes[q][3];
        outputWidth = A.bounding_boxes[q][4];
        outputHeight = A.bounding_boxes[q][5];

        tempBox.push_back(cornerX);
        tempBox.push_back(cornerY);
        tempBox.push_back(affectedWidth);
        tempBox.push_back(affectedHeight);
        tempBox.push_back(outputWidth);
        tempBox.push_back(outputHeight);
        boxes.push_back(tempBox);
      }
    }

    //std::cout << "Corner (" << cornerX << "," << cornerY << ")\n";
    //std::cout << "Size is " << affectedWidth << " " << affectedHeight << std::endl;

    //if(A.dims() != 2 || B.dims() != 1 || A.dim(1) != B.dim(0)) {
    if(A.dims() == 0 || B.dims() != 1 || A.dim(A.dims()-1) != B.dim(0)) {
      for(int i = 0; i < A.dims(); i++) std::cout << A.dim(i) << ' ';
      std::cout << '\n';
      for(int i = 0; i < B.dims(); i++) std::cout << B.dim(i) << ' ';
      std::cout << '\n';
      fatal_error("Dimension Mismatch in " + name());
    }
    TensorContainer out(DTypeMap<T>::type, A.shape());
    const T *arr_A = A.ptr();
    const T *arr_B = B.ptr();
    if(mode == 1 || mode == 2)  
    {
      T *arr_out = TensorExtractor<T>::Get(out)->ptrInit();
      const size_t stride_len = A.dim(A.dims()-1);
      const size_t strides = A.elements() / stride_len;
      #pragma omp parallel for num_threads(parallelism)
      for(size_t r = 0; r < strides; r++) {
        const T *a_start = arr_A + r * stride_len;
        T *out_start = arr_out + r * stride_len;
        for(size_t c = 0; c < stride_len; c++) {
          out_start[c] = a_start[c] + arr_B[c];
        }
      }
    }

    if (mode == 2) {
      Tensor<float>& out_data = *(out.t_float);
      out_data.bounding_boxes.clear();
      for (int q = 0; q < boxes.size(); q++) {
        out_data.bounding_boxes.push_back(boxes[q]);
      }
    }
    return {out};
  }

#ifdef USE_CUDA
  make_binary_gpucompute(float);
  make_binary_gpucompute(int);
  make_binary_gpucompute(long);

  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer> &args, uint32_t * workIndex) override {
    if(args.size() != 2) fatal_error("Incorrect arg count");
    switch(args[0].dtype) {
    case DT_FLOAT: return GPUCompute(*(args[0].t_float), *(args[1].t_float), workIndex);
    case DT_INT: return GPUCompute(*(args[0].t_int), *(args[1].t_int), workIndex);
    default:
      fatal_error(name());
      return std::vector<TensorContainer>();
    }
  }

  template<typename T>
  TensorContainer GPUKernel(Tensor<T> &A, Tensor<T> &B, uint32_t * workIndex) override;
#endif
};

    /* else if (mode == 2)
    {
      T *arr_out = TensorExtractor<T>::Get(op_output)->ptrInit();
      const size_t stride_len = A.dim(A.dims()-1);
      int depth = A.elements() / (outputWidth*outputHeight); 
      //int firstStart = cornerY*layerWidth*depth;
      //int firstEnd = (cornerY+affectedHeight)*layerWidth*depth;
      //int secondStart = cornerX*depth;
      //int secondEnd = (cornerX+affectedWidth)*depth;
      #pragma omp parallel for num_threads(parallelism)
      for(int i = cornerY; i < cornerY+affectedHeight; i++) { 
      //for(int i = firstStart; i < firstEnd; i++) { 
        for(int j = cornerX; j <  cornerX+affectedWidth; j++) { 
        //for(int j = secondStart; j <  secondEnd; j++) { 
          for(int k = 0; k < depth; k++) { 
            int spot = (i*outputWidth*depth)+(j*depth)+k; 
            arr_out[spot] = arr_A[spot] + arr_B[spot%stride_len]; 
          } 
        } 
      } 
    } */

    //TODO what is goin on here...?
    /* if (mode == 1)
    {
      op_output = out;
    }
    else if (mode == 2)
    {
      out = op_output;
    }*/

        //std::cout << "A Corner (" << cornerX << "," << cornerY << ")\n";
        //std::cout << "A Size is " << affectedWidth << " " << affectedHeight << std::endl;
        /*if (B.bounding_boxes.size() > 0 && B.bounding_boxes.size() > q)
        {
          if (B.bounding_boxes[q][0] < cornerX) {
            cornerX = B.bounding_boxes[q][0];
          }

          if (B.bounding_boxes[q][1] < cornerY) {
            cornerY = B.bounding_boxes[q][1];
          }

          if (B.bounding_boxes[q][2] > affectedWidth) {
            affectedWidth = B.bounding_boxes[q][2];
          }

          if (B.bounding_boxes[q][3] > affectedHeight) {
            affectedHeight = B.bounding_boxes[q][3];
          }
        }*/
