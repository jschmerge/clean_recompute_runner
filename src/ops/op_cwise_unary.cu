#ifdef USE_CUDA

#include "op_cwise_unary.hpp"

#include "cuda.h"
#include "cuda_runtime.h"
#include "cuda_runtime_api.h"

#define BLOCK_SIZE 256
#define NSMS 20
#define MAX_BLOCKS 8
#define K 1

template<typename T, typename func>
__global__
void unary_op(const T * in, T * out, size_t size, uint32_t mask, func f, uint32_t * workIndex) {

  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if ( !(mask & (1 << smid))) return;

  int tid = threadIdx.x;
  int bDim = blockDim.x;

  __shared__ size_t grabbedIndex;
  while(true) {
    if (tid == 0) {
      grabbedIndex = atomicAdd(workIndex, bDim * K);
    }

    __syncthreads();

    if ( grabbedIndex >= size) return;

    #pragma unroll
    for (size_t i = 0; i < K; i++) {
      if ( grabbedIndex + tid + bDim * i < size) {
        out[grabbedIndex + tid + bDim * i] = f(in[grabbedIndex + tid + bDim * i]);
      } else {
        return;
      }
    }
  }
}

template<class func, typename T>
void launch_unary_op(const T * in, T * out, size_t size, uint32_t mask, func f, uint32_t * workIndex) {
  unary_op<T, func><<<NSMS*MAX_BLOCKS, BLOCK_SIZE, 0>>>(in, out, size, mask, f, workIndex);
}

#define create_unary_templates(name)                                    \
template void launch_unary_op<name, float> (const float *, float *, size_t, uint32_t, name, uint32_t *);  \
template void launch_unary_op<name, int> (const int *, int *, size_t, uint32_t, name, uint32_t *);  \
template void launch_unary_op<name, long> (const long *, long *, size_t, uint32_t, name, uint32_t *);  

create_unary_templates(SigmoidFunctor)
create_unary_templates(TanhFunctor)
create_unary_templates(RsqrtFunctor)
create_unary_templates(ReluFunctor)

#endif
