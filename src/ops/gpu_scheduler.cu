#include <strings.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <iostream>

#include "common.h"

uint32_t GPUScheduler::occupied = 0; 

uint32_t GPUScheduler::n_sms = 0;

std::mutex GPUScheduler::sm_lock;

uint32_t GPUScheduler::schedule_sms(int parallelism) {
  std::lock_guard<std::mutex> lock(sm_lock);
  if ( !n_sms ) {
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, 0);
    n_sms = prop.multiProcessorCount;
    for (size_t i = 0; i < 32 - n_sms; i++) {
      occupied += 1;
      occupied = occupied << 1;
    }
    occupied = occupied << n_sms;
  }
  uint32_t inv = ~occupied;
  uint32_t mask = 0;
  while (parallelism > 0) {
    int ind = ffs(inv);
    if (!ind) fatal_error("Attempted to over subscribed GPU cores"); 
    mask |= (1 << (ind - 1));
    inv ^= (1 << (ind -1));
    parallelism--;
  }
  occupied |= mask;
  return mask;
}

void GPUScheduler::return_sms(uint32_t mask) {
  std::lock_guard<std::mutex> lock(sm_lock);
  occupied ^= mask;
}
