#pragma once
#include <cstring>
#include <iostream>
#include <cassert>

#include "common.h"

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#endif

class GatherOp : public Op {
  using Op::Op;
public:
  std::vector<TensorContainer> Compute(const std::vector<TensorContainer>& args) override {
    std::cout << "In Gather \n";
    assert(args.size() == 2);
    assert(args[0].dtype == DT_FLOAT);
    if(args[1].dtype == DT_INT) return { Kernel(*(args[0].t_float), *(args[1].t_int)) };
    else if(args[1].dtype == DT_LONG) return { Kernel(*(args[0].t_float), *(args[1].t_long)) };
    else fatal_error("Unsupported dtype");
  }
  template<typename T1, typename T2>
  TensorContainer Kernel(Tensor<T1> &A, Tensor<T2> &B) {
    std::vector<size_t> out_dims(B.shape());
    out_dims.insert(out_dims.end(), A.shape().begin()+1, A.shape().end());
    size_t stride = 1;
    for(size_t i = 1; i < A.shape().size(); i++) {
      stride *= A.shape()[i];
    }
    TensorContainer out(DTypeMap<T1>::type, out_dims);
    Tensor<T1> *t_out = TensorExtractor<T1>::Get(out);
    T1 *p_out = t_out->ptrInit();
    const T1 *p_A = A.ptr();
    const T2 *p_B = B.ptr();
    #pragma omp parallel for num_threads(parallelism)
    for(size_t i = 0; i < B.elements(); i++) {
      assert(p_B[i] < A.dim(0));
      memcpy(p_out+stride*i, p_A+p_B[i]*stride, stride*sizeof(T1)); 
    }
    return out;
  }

#ifdef USE_CUDA
  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer>& args, uint32_t * workIndex) override {
    assert(args.size() == 2);
    assert(args[0].dtype == DT_FLOAT);
    if(args[1].dtype == DT_INT) return { GPUKernel(*(args[0].t_float), *(args[1].t_int), workIndex) };
    else if(args[1].dtype == DT_LONG) return { GPUKernel(*(args[0].t_float), *(args[1].t_long), workIndex) };
    else fatal_error("Unsupported dtype");
  }

  template<typename T1, typename T2>
  TensorContainer GPUKernel(Tensor<T1> &A, Tensor<T2> &B, uint32_t * workIndex);

#endif

};

