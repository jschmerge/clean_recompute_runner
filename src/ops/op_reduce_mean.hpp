#pragma once

#include "common.h"

#include <omp.h>

class ReduceMeanOp : public BinaryOp {
  using BinaryOp::BinaryOp;
public:
  make_binary_compute(float);
  make_binary_compute(int);
  make_binary_compute(long);
  template<typename T>
  std::vector<TensorContainer> Kernel(Tensor<T> &A, Tensor<T> &B) {
    std::vector<T> sum(parallelism, 0.0);
    const T *ptr = A.ptr();
    const size_t elements = A.elements();
    #pragma omp parallel for num_threads(parallelism)
    for(size_t i = 0; i < elements; i++) {
      sum[omp_get_thread_num()] += ptr[i];
    }
    for(int i = 1; i < parallelism; i++) {
      sum[0] += sum[i];
    }
    TensorContainer out(DTypeMap<T>::type, {1});

    TensorExtractor<T>::Get(out)->ptrInit()[0] = sum[0] / elements;

    return {out};
  }

#ifdef USE_CUDA
  make_binary_gpucompute(float);
  make_binary_gpucompute(int);
  make_binary_gpucompute(long);

  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer> &args, uint32_t * workIndex) override {
    if(args.size() != 2) fatal_error("Incorrect arg count");
    switch(args[0].dtype) {
      case DT_FLOAT: return GPUCompute(*(args[0].t_float), *(args[1].t_float), workIndex);
      case DT_INT: return GPUCompute(*(args[0].t_int), *(args[1].t_int), workIndex);
      default:
        fatal_error(name());
        return std::vector<TensorContainer>();
    }
  }
  
  template<typename T>
  TensorContainer GPUKernel(Tensor<T> &A, Tensor<T> &B, uint32_t * workIndex);
  
#endif
};

