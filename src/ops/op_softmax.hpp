#pragma once
#include <cmath>
#include <cassert>
#include <vector>

#include "common.h"

class SoftmaxOp : public UnaryOp {
  using UnaryOp::UnaryOp;
public:
  make_unary_compute(float);
  make_unary_compute(int);
  make_unary_compute(long);
  template<typename T>
  std::vector<TensorContainer> Kernel(Tensor<T> &A) {
    assert(A.dims() == 2);
    TensorContainer out(DTypeMap<T>::type, A.shape());
    const T *arr_A = A.ptr();
    T *arr_out = TensorExtractor<T>::Get(out)->ptrInit();
    std::vector<T> max(A.dim(0), 0.0);
    //#pragma omp parallel for num_threads(parallelism)
    for(size_t r = 0; r < A.dim(0); r++) {
      const T *start_A = arr_A + r * A.dim(1);
      for(size_t c = 0; c < A.dim(1); c++) {
        if(start_A[c] > max[r]) max[r] = start_A[c];
      }
    }
    std::vector<double> sum(A.dim(0), 0.0);
    //#pragma omp parallel for num_threads(parallelism)
    for(size_t r = 0; r < A.dim(0); r++) {
      const T *start_A = arr_A + r * A.dim(1);
      T *start_out = arr_out + r * A.dim(1);
      for(size_t c = 0; c < A.dim(1); c++) {
        start_out[c] = exp(start_A[c] - max[r]);
        sum[r] += start_out[c];
      }
    }
    //#pragma omp parallel for num_threads(parallelism)
    for(size_t r = 0; r < A.dim(0); r++) {
      T *start_out = arr_out + r * A.dim(1);
      for(size_t c = 0; c < A.dim(1); c++) {
        start_out[c] /= sum[r];
      }
    }
    return {out};
  }

#ifdef USE_CUDA
  make_unary_gpucompute(float);
  make_unary_gpucompute(int);
  make_unary_gpucompute(long);

  std::vector<TensorContainer> GPUCompute(const std::vector<TensorContainer> &args, uint32_t * workIndex) override {
    if(args.size() != 1) fatal_error("Incorrect arg count");
    switch(args[0].dtype) {
      case DT_FLOAT: return GPUCompute(*(args[0].t_float), workIndex);
      case DT_INT: return GPUCompute(*(args[0].t_int), workIndex);
      default:
        fatal_error(name());
        return std::vector<TensorContainer>();
    }
  }

  template<typename T>
  TensorContainer GPUKernel(Tensor<T> &A, uint32_t * workIndex) override;
#endif
};


