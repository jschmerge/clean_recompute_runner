#ifdef USE_CUDA

#include "op_cwise_binary.hpp"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stdio.h>

#define BLOCK_SIZE 256
#define NSMS 20
#define MAX_BLOCKS 8
#define K 1


template<typename T1, typename T2>
__global__
void binary_op(const T1 * in1, const T1 * in2, T1 * out, size_t size, uint32_t mask, T2 func, uint32_t * workIndex) {
  
  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if ( !(mask & (1 << smid))) return;
  
  int tid = threadIdx.x;
  int bDim = blockDim.x;
  
  __shared__ size_t grabbedIndex;
  while(true) {
    if ( tid == 0 ) {
      grabbedIndex = atomicAdd(workIndex, bDim * K);
    }
    
    __syncthreads(); 
    if ( grabbedIndex >= size) return;
    //if (tid == 0) printf("%u\n", get_smid());
    for ( size_t i = 0; i < K; i++) {
      if ( grabbedIndex + tid + bDim * i < size) {

        out[grabbedIndex + tid + bDim * i] = func(in1[grabbedIndex + tid + bDim * i],
                                                  in2[grabbedIndex + tid + bDim * i]);
      } else {
        return;
      }
    }
  }

}

template<typename T1, typename T2>
__global__
void binary_op(T1 in1, const T1 * in2, T1 * out, size_t size, uint32_t mask, T2 func, uint32_t * workIndex) {
  
  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if ( !(mask & (1 << smid))) return;
  
  int tid = threadIdx.x;
  int bDim = blockDim.x;
  
  __shared__ size_t grabbedIndex;
  while(true) {
    if ( tid == 0 ) {
      grabbedIndex = atomicAdd(workIndex, bDim * K);
    }
    
    __syncthreads(); 
    if ( grabbedIndex >= size) return;
    //if (tid == 0) printf("%u\n", get_smid());
    for ( size_t i = 0; i < K; i++) {
      if ( grabbedIndex + tid + bDim * i < size) {

        out[grabbedIndex + tid + bDim * i] = func(in1,
                                                  in2[grabbedIndex + tid + bDim * i]);
      } else {
        return;
      }
    }
  }

}

template<typename T1, typename T2>
__global__
void binary_op(const T1 * in1, T1 in2, T1 * out, size_t size, uint32_t mask, T2 func, uint32_t * workIndex) {
  
  uint smid;
  asm("mov.u32 %0, %smid;" : "=r"(smid));
  if ( !(mask & (1 << smid))) return;
  
  int tid = threadIdx.x;
  int bDim = blockDim.x;
  
  __shared__ size_t grabbedIndex;
  while(true) {
    if ( tid == 0 ) {
      grabbedIndex = atomicAdd(workIndex, bDim * K);
    }
    
    __syncthreads(); 
    if ( grabbedIndex >= size) return;
    //if (tid == 0) printf("%u\n", get_smid());
    for ( size_t i = 0; i < K; i++) {
      if ( grabbedIndex + tid + bDim * i < size) {

        out[grabbedIndex + tid + bDim * i] = func(in1[grabbedIndex + tid + bDim * i],
                                                  in2);
      } else {
        return;
      }
    }
  }

}

template<class func, typename T>
void launch_binary_op(const T * in1, const T * in2, T * out, size_t size, uint32_t mask, func f, uint32_t * workIndex) {
  binary_op<T, func><<<NSMS*MAX_BLOCKS, BLOCK_SIZE, 0>>>(in1, in2, out, size, mask, f, workIndex);
}

template<class func, typename T>
void launch_binary_op(const T * in1, const T in2, T * out, size_t size, uint32_t mask, func f, uint32_t * workIndex) {
  binary_op<T, func><<<NSMS*MAX_BLOCKS, BLOCK_SIZE, 0>>>(in1, in2, out, size, mask, f, workIndex);
}

template<class func, typename T>
void launch_binary_op(const T in1, const T * in2, T * out, size_t size, uint32_t mask, func f, uint32_t * workIndex) {
  binary_op<T, func><<<NSMS*MAX_BLOCKS, BLOCK_SIZE, 0>>>(in1, in2, out, size, mask, f, workIndex);
}

#define create_binary_templates(name)                                                                       \
template void launch_binary_op<name, float>(const float *, const float *, float *, size_t, uint32_t, name, uint32_t *); \
template void launch_binary_op<name, float>(const float, const float *, float *, size_t, uint32_t, name, uint32_t *);  \
template void launch_binary_op<name, float>(const float *, const float, float *, size_t, uint32_t, name, uint32_t *);  \
                                                                                 \
template void launch_binary_op<name, int>(const int *, const int *, int *, size_t, uint32_t, name, uint32_t *); \
template void launch_binary_op<name, int>(const int, const int *, int *, size_t, uint32_t, name, uint32_t *);  \
template void launch_binary_op<name, int>(const int *, const int, int *, size_t, uint32_t, name, uint32_t *);  \
                                                                                 \
template void launch_binary_op<name, long>(const long *, const long *, long *, size_t, uint32_t, name, uint32_t *); \
template void launch_binary_op<name, long>(const long, const long *, long *, size_t, uint32_t, name, uint32_t *);  \
template void launch_binary_op<name, long>(const long *, const long, long *, size_t, uint32_t, name, uint32_t *);  

create_binary_templates(SubFunctor);
create_binary_templates(AddFunctor);
create_binary_templates(MulFunctor);
create_binary_templates(EqualFunctor);

#endif
