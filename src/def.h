#pragma once

#include <vector>
#include <string>
#include <atomic>
#include <istream>
#include <ostream>

#ifdef USE_CUDA
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#endif

#define fatal_error(msg) { localized_message((msg), __FILE__, __LINE__); exit(1); }
void localized_message(std::string msg, const char *file, int line);

enum TensorType {
  TT_NORMAL,
  TT_SCALAR,
  TT_NONE
};

enum TensorDType {
  DT_FLOAT,
  DT_DOUBLE,
  DT_INT,
  DT_LONG,
  DT_STRING,
  DT_NONE
};

enum TargetDevice {
  CPU,
  GPU
};

#ifdef USE_CUDA
enum DataState {
  UNALLOCATED,
  INVALID,
  RESIDENT
};
#endif

template <typename T>
struct DTypeMap { static const TensorDType type; };

std::string dtype_string(TensorDType);
TensorDType string_dtype(std::string);

size_t elements_count(std::vector<size_t> dims);

template<typename T>
class Tensor {
private:
  const std::vector<size_t> dimensions;

  T * h_data = nullptr;

  T scalar_val;

  size_t num_elements;

  bool init = false;
#ifdef USE_CUDA
  T * d_data = nullptr;

  DataState h_state;

  DataState d_state;

#endif

public:
  std::vector<std::vector<int> > bounding_boxes;
  /*int cornerX;
  int cornerY;
  int affectedWidth;
  int affectedHeight;
  int layerWidth;
  int layerHeight;*/

  explicit Tensor(const std::vector<size_t> &in_dimensions);

  explicit Tensor(const T val);

  ~Tensor();

  const std::vector<size_t> &shape() const;

  size_t dims() const;

  size_t dim(size_t idx) const;

  size_t elements() const;

#ifdef USE_CUDA
  T *ptrInit(TargetDevice dev = CPU, cudaStream_t * stream = nullptr);

  const T *ptrShr(TargetDevice dev = CPU, cudaStream_t * stream = nullptr);

  const T *ptr(TargetDevice dev = CPU, cudaStream_t * stream = nullptr);
#else
  T *ptrInit(int n = 0);

  const T *ptrShr(int n = 0);

  const T *ptr(int n = 0);
#endif

  // The get functions should only be called for inputs to a Tensorflow graph
  // Get when called on a Tensor outputted from an op may be undefined.
  T &get(const size_t idx);

  const T &get(const size_t idx) const;

  T &get(const size_t idx, const size_t idx2);

  const T &get(const size_t idx, const size_t idx2) const;

  const T &val() const;
};

class TensorContainer {
public:
  TensorDType dtype;
  TensorType ttype;

  std::shared_ptr<Tensor<float>> t_float;
  std::shared_ptr<Tensor<int>> t_int;
  std::shared_ptr<Tensor<long>> t_long;
  std::shared_ptr<Tensor<std::string>> t_string;

  TensorContainer();

  template<typename T>
  TensorContainer(const T);

  TensorContainer(TensorDType, std::vector<size_t>);

  std::vector<size_t> shape() const;
  size_t elements() const;

  friend std::ostream& operator<<(std::ostream&, TensorContainer&);
  friend std::istream& operator<<(std::istream&, TensorContainer&);
};

template<typename T>
struct TensorExtractor {
  static Tensor<T> *Get(TensorContainer &tc);
  static const Tensor<T> *Get(const TensorContainer &tc);
};

