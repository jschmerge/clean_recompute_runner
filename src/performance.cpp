#include <iostream>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <omp.h>
#include <mkl.h>

#include "graph.h"
#include "load.h"
#include "ops/register.h"

int main(int argc, char** argv) {
  RegisterOps();
  if(argc != 2) {
    std::cerr << "usage: ./run_graph <graph.def>\n";
    exit(1);
  }

  omp_set_nested(1);
  omp_set_dynamic(0);
  omp_set_max_active_levels(2);
  mkl_set_dynamic(0);

  OpGraph op_graph = load_graph(argv[1]);

	for(int i = 0; i < 100; i++) {
    op_graph.run_mt2();
    op_graph.reset();
  }
  for(int iters = 0; iters < 1000; iters++) {
    auto t1 = std::chrono::high_resolution_clock::now();
    op_graph.run_mt2();
    auto t2 = std::chrono::high_resolution_clock::now();
    unsigned delta = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();
    std::cout << delta << " microseconds\n";
    op_graph.reset();
  }

  return 0;
}
