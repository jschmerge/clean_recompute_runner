#pragma once

#include <string>
#include <unordered_map>
#include <atomic>
#include <mutex>
#include <map>

#include "ops/common.h"

// TODO generalize for devices
class AffinityControl {
private:
  static const int MAX_CORES;
  static std::mutex lock;
  static std::vector<bool> global_mask;
public:
  std::vector<bool> local_mask;
  AffinityControl();
  void set_affinity(int parallelism);
  void set_affinity(int parallelism, const std::vector<bool> &preferred);
  void clear_affinity();
};

class OpInfo {
public:
  std::shared_ptr<Op> op;
  std::vector<std::tuple<OpInfo*, unsigned, unsigned>> outgoing;
  std::vector<OpInfo*> outgoing_control;
  std::vector<TensorContainer> inputs, start_state;
  std::atomic<uint32_t> unsatisfied;
  unsigned start_unsatisfied, control_unsatisfied;
  AffinityControl affinity_control;
  OpInfo();
  OpInfo(const std::string op_name, const std::string instance_name, const int parallelism);
  OpInfo(const OpInfo &other);
  ~OpInfo();
  void save_start_state();
  void reset();
};

class OpGraph {
private:
  std::unordered_map<std::string, OpInfo> info;
  std::vector<std::pair<std::string, TensorContainer>> outputs;
  std::map <std::string, TensorContainer> check_data;
  TensorContainer oldInput;
  unsigned num_inputs;
public:
  OpGraph();

  void add_op(std::string op_name, std::string instance_name, int parallelism);

  void add_data_edge(std::string one, std::string two, unsigned idx, unsigned from_idx);

  template<typename T1, typename T2>
  void add_data_edge(const T1 &one, const T2 &two, unsigned idx, unsigned from_idx);

  void add_control_edge(std::string one, std::string two);

  template<typename T1, typename T2>
  void add_control_edge(const T1 &one, const T2 &two);

  void add_input(std::string op, unsigned idx, TensorContainer &mat);

  template<typename T>
  void add_input(const T &op, unsigned idx, TensorContainer &mat);

  void prepare();

  void reset();

  std::vector<std::string> get_op_names();
  std::vector<std::pair<std::string, int>> get_input_names(std::string);
  std::vector<TensorContainer> get_initialized_input(std::string);
  Op &get_op(std::string);

  int get_inter();

  template<typename T>
  static void print_tensor(const Tensor<T> &A);

  template<typename T>
  void write_tensor(std::string filename, Tensor<T> &A);

  //float* blur (const float* inputTensor, int input_height, int input_width, int input_depth);

  void write_outputs_to_file(std::string filename);
  void print_outputs();

  template<typename T>
  double euclidean(TensorContainer one, TensorContainer two);
  double get_error(TensorContainer one, TensorContainer two);

  void change_input(std::string op, unsigned idx, TensorContainer &mat);

  void run_st(bool firstRun);

  void run_mt(bool firstRun);
  void run_mt2();

  void bench_scalability(int max_threads);

};

