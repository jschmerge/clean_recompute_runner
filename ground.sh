#!/bin/bash

method='original' #original, strided, or recompute
num_images=15 #total number of frames we want to run on
stride=1 #stride between images
startingFrame=0 #Where to start
video='Parking'
#video='Town'
model='Inception'
#model='Resnet'

#Execution of the model
if [ $model == 'Inception' ]
then
  numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $method $num_images $stride $startingFrame $video $model
fi

if [ $model == 'Resnet' ]
then
  numactl --cpubind=3 --membind=3 ./bin/run_graph models/newresnet50/model $method $num_images $stride $startingFrame $video $model
fi

for (( i=startingFrame; i<=num_images; i++ ))
do
  file="$model"Results/ground"$i".txt
  newfile="$model"Results/g_truth"$i".txt

  cat $file | tr ' ' '\n' > $newfile
  cat $newfile | grep -v "^$" > $file

  mv $file "$model"Results/"$video"Ground/
  rm $newfile

done
