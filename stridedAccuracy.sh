#!/bin/bash

#SET PARAMS TO GET INITIAL STRIDE ACCURACY FOR BASELINE
method='strided' #original, strided, or recompute
num_images=499 #total number of frames we want to run on
#num_images=10 #total number of frames we want to run on
groundStride=1 #stride for "ground truth"
#video='Parking'
video='Town'
model='Inception'
#model='Resnet'

echo "Strided Approach" > "$model""$video"Results/strResults.txt

for (( q=1; q<11; q+=1 ))
do
  echo Doing stride $q
  currentStridedFrame=0 #The frame we are using
  startingFrame=0 #Where to start
  strideStride=$q #stride between images
  strideTime=0
  #Execution of the model
  if [ $model == 'Inception' ]
  then
    numactl --cpubind=3 --membind=3 ./bin/run_graph models/inception/model $method $num_images $strideStride $startingFrame $video $model
  fi

  if [ $model == 'Resnet' ]
  then
    numactl --cpubind=3 --membind=3 ./bin/run_graph models/newresnet50/model $method $num_images $strideStride $startingFrame $video $model
  fi

  #Do automatic performance analysis
  strideTime=$(awk 'BEGIN{a=0} {a += $1} END{printf "%.1f", a}' orgTimings.txt)
  #echo The total run time for the strided approach is $strideTime microseconds

  #Can do accuracy checks if so desired
  avgStridedError=0
  accError=0
  for (( i=0; i<=num_images; i+=groundStride ))
  do
    ground="$model"Results/"$video"Ground/ground"$i".txt
    if (( $i % strideStride == 0 ))
    then
      currentStridedFrame=$i
      base="$model"Results/str$i.txt
      temp="$model"Results/strided"$i".txt
      cat $base | tr ' ' '\n' > $temp
      cat $temp | grep -v "^$" > $base
      rm $temp
    fi
    errorTop=$(awk 'BEGIN{acc=0}{getline t<'\"$ground\"'; tmp = $0-t; acc += tmp * tmp;}END{printf "%.20f", acc}' $base)
    errorBot=$(awk 'BEGIN{acc=0}{tmp = $0; acc += tmp * tmp;}END{printf "%.20f", acc}' $ground)
    error=$(python3 -c "print ($errorTop**(1/2.0)/($errorBot**(1/2.0)))")
    echo Error between $ground and $base is $error
    accError=$(python3 -c "print ($accError + $error)")
  done
  finalError=$(python3 -c "print ($accError/(($num_images/$groundStride)+1))")
  #echo The average error for all frames is $finalError

  #echo $q $strideTime $finalError
  echo $q $strideTime $finalError >> "$model""$video"Results/strResults.txt
done
